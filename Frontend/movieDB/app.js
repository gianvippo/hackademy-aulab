const puppeteer = require('puppeteer');
const fs = require('fs')

async function scraper(url) {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto(url)


  const data = await page.evaluate(() => {
    // Create array to store objects:
    const list = [];
    // Get each product container:
    const items = document.querySelectorAll(".lister-list > tr");

    // Each iteration of loop then pushes an object to the array:

    items.forEach((item, index) => {
      list.push({
        position: index + 1,
        id: item.querySelector(".ratingColumn > div").getAttribute('data-titleid'),
        title: item.querySelector(".titleColumn > a").textContent,
        year: Number(item.querySelector(".titleColumn > span").textContent.replace("(", "").replace(")", ""))
      })
    })

    // Return the array of objects (console.log here will print in the browser!)
    return list
  })

  console.log(data); // See the scraped data in Node

  // convert JSON object to a string
  const result = JSON.stringify(data)

  // write JSON string to a file
  fs.writeFile('movies.json', result, err => {
    if (err) {
      throw err
    }
    console.log('JSON data is saved.')
  })

}

scraper("https://www.imdb.com/chart/top/?ref_=nv_mv_250")