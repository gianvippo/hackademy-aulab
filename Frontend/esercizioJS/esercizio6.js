let i = 0;

while (i==0) {

let num = prompt("Inserisci un numero corrispondente al giorno della settimana:");

if (isNaN(num) == false) {

    if (num > 0 && num < 8) {

        if (num == 1) alert(`Il numero ${num} corrisponde al giorno di lunedì`)

        if (num == 2) alert(`Il numero ${num} corrisponde al giorno di martedì`)

        if (num == 3) alert(`Il numero ${num} corrisponde al giorno di mercoledì`)

        if (num == 4) alert(`Il numero ${num} corrisponde al giorno di giovedì`)

        if (num == 5) alert(`Il numero ${num} corrisponde al giorno di venerdì`)

        if (num == 6) alert(`Il numero ${num} corrisponde al giorno di sabato`)

        if (num == 7) alert(`Il numero ${num} corrisponde al giorno di domenica`)

        i++;

    } else alert("Numero non valido, riscrivi")

} else alert("Non hai inserito un numero, riscrivi")

}