let z = 0;
let c = 0;
let stringa = new Array();
let average = 0;
let minNumb = new Array();
let maxNumb = new Array();

while (z == 0) {

   let num = prompt("Inserire il numero di elementi da inserire nell'array:");

   if (isNaN(num) == false) {

      for (let i = 0; i < num; i++) {

         c = 0;

         while (c == 0) {

            let a = prompt(`Inserire il numero da inserire nell'array nella posizione ${i + 1}:`)

            if (isNaN(a) == false) {
               
               a = parseInt(a)
               stringa.push(a)
               c++;

            } else alert("Non hai inserito un numero, riscrivi")

         }

      }

      average = stringa.reduce((x, y) => x + y, 0) / stringa.length
      minNumb = stringa.filter( x => x < average)
      maxNumb = stringa.filter( x => x >= average)

      
      console.log(`L'input di numeri inseriti è ${stringa.join(", ")}`)
      console.log(`La media tra quelli inseriti è ${average}`)
      console.log(`I numeri minori della media sono ${minNumb.join(", ")} e sono ${minNumb.length} numeri`)
      console.log(`I numeri maggiore della media sono ${maxNumb.join(", ")} e sono ${maxNumb.length} numeri`)

      z++;

   } else alert("Non hai inserito un numero, riscrivi")

}