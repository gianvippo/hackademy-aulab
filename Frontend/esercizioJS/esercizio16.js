let numeroTiri = 0;
let z = 0;

let giocatori = [
    {
        player: "Giocatore 1",
        punteggio: 0
    },
    {
        player: "Giocatore 2",
        punteggio: 0
    },
]

function lancioDadi(x,y) {

    console.log(x.player)

    for (let i = 0; i < y; i++) {

        let dado = Math.floor(Math.random() * (6 - 1 + 1) + 1)
        console.log(`Lancio ${i+1}: ${dado}`)
        x.punteggio += dado
        
    }

    return x.punteggio

}

while (z == 0) {

    let num = prompt("Inserire il numero di lanci:");
 
    if (isNaN(num) == false) {

    console.log(`Somma ${giocatori[0].player}: ${lancioDadi(giocatori[0], num)}`)
    console.log(`Somma ${giocatori[1].player}: ${lancioDadi(giocatori[1], num)}`)

    if(giocatori[0].punteggio > giocatori[1].punteggio) console.log(`Ha vinto il ${giocatori[0].player}`)
    else if (giocatori[0].punteggio == giocatori[1].punteggio) console.log(`Pareggio`)
    else console.log(`Ha vinto il ${giocatori[1].player}`)
       
       z++;
 
    } else alert("Non hai inserito un numero, riscrivi")
 
 }