let z = 0;
let c = 0;
let stringa = new Array();

while (z == 0) {

   let num = prompt("Inserire il numero di elementi da inserire nell'array:");

   if (isNaN(num) == false) {

      for (let i = 0; i < num; i++) {

         c = 0;

         while (c == 0) {

            let a = prompt(`Inserire il numero da inserire nell'array nella posizione ${i + 1}:`)

            if (isNaN(a) == false) {
               
               stringa.push(a)
               c++;

            } else alert("Non hai inserito un numero, riscrivi")

         }

      }
      
      console.log(`Il numero minore tra quelli inseriti è ${Math.min(...stringa)}`)
      console.log(`Il numero maggiore tra quelli inseriti è ${Math.max(...stringa)}`)

      z++;

   } else alert("Non hai inserito un numero, riscrivi")

}