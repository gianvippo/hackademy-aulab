let c = 0;
let stringa = new Array();
let stringaRev = new Array();
let stringaSort = new Array();

for (let i = 0; i < 10; i++) {

   c = 0;

   while (c == 0) {

      let a = prompt(`Inserire il numero da inserire nell'array nella posizione ${i + 1}:`)

      if (isNaN(a) == false) {

         a = parseInt(a)
         stringa.push(a)
         c++;

      } else alert("Non hai inserito un numero, riscrivi")

   }

}

console.log(`L'input di numeri inseriti è ${stringa.join(", ")}`)
console.log(`La stringa ordinata in modo decrescente è ${stringa.sort((x,y) => y-x).join(", ")}`)
console.log(`La stringa ordinata in modo crescente è ${stringa.sort((x,y) => x-y).join(", ")}`)