let garage = {

    lista: [
        {
            marca: "Fiat",
            modelli: ["Punto", "Panda", "500", "Tipo"]
        },

        {
            marca: "Volkswagen",
            modelli: ["Golf", "Polo", "Up", "Passat"]
        },
        {
            marca: "Seat",
            modelli: ["Leon", "Ibiza", "Arona", "Altea"]
        },
        {
            marca: "Lancia",
            modelli: ["Delta", "Lybra", "Ypsilon", "Dedra"]
        },
    ]

}

let marca = 0;

function controllo(x, input) {

    let index = -1;

    x.lista.forEach((valore, i) => {
        if(valore.marca == input)
            index = i;       
    })

    return index

}

marca = prompt(`Inserisci la marca:`);

if(controllo(garage, marca)>=0) console.log(`La marca ${marca} è presente nel garage. I modelli sono ${garage.lista[controllo(garage, marca)].modelli.join(", ")}`)
else console.log(`La marca non è presente nel garage.`)