fetch("./MOCK_DATA.json")
.then((response) => response.json())
.then(dati => {
    console.log(dati);
    
    let somma = 0;
    let sommaEntrate = 0;
    let sommaUscite = 0;
    let ultimeTransazioni = dati;
    
    let totale = document.querySelector("#totale");
    let entrate = document.querySelector("#entrate")
    let uscite = document.querySelector("#uscite")
    let ultime = document.querySelector("#ultime")
    
    dati.forEach(element => {
        somma += element["amount "];
    });
    
    totale.innerHTML = `${somma.toFixed(2)} $`
    
    if(somma < 0){
        totale.classList.add("text-danger");
    }
    
    dati.forEach(element => {
        if(element["amount "] > 0){
            sommaEntrate += element["amount "];
        }
        else{
            sommaUscite += element["amount "];
        }
    })
    
    entrate.innerHTML = `+${sommaEntrate.toFixed(2)} $`
    uscite.innerHTML = `${sommaUscite.toFixed(2)} $`
    
    let ultime5 = ultimeTransazioni.sort((a,b) => {
        return new Date(b.operation_date) - new Date(a.operation_date)
    }).slice(0, 5)
    
    console.log(ultime5);
    
    let cardBody = document.querySelector("#card-body")
    
    
    ultime5.forEach((el) => {
        let row = document.createElement("div")
        row.classList.add("row", "p-2", "mb-1", "justify-content-between")
        
        if(el.operation_category == "n/a"){
            el.operation_category = "Non categorizzato"
        }
        
        if(el["amount "] > 0){
            row.classList.add("border", "border-2", "border-success", "rounded-3")
            row.innerHTML += `
            <div class="col-4 col-md-1 mb-2 mb-md-0 d-flex align-items-center justify-content-center">
            <img src = "./up.png" height = "30px">
            </div>`
        }
        else{
            row.classList.add("border", "border-2", "border-danger", "rounded-3")
            row.innerHTML += `
            <div class="col-4 col-md-1 mb-2 mb-md-0 d-flex align-items-center justify-content-center">
            <img src = "./down.png" height = "30px">
            </div>`
        }
        
        row.innerHTML += `
        <div class="col-4 col-md-2 mb-2 mb-md-0">
        <p class = "m-0 p-0">Data</p>
        <p class = "m-0 p-0">${el.operation_date}</p>
        </div>
        <div class="col-4 col-md-2 d-flex align-items-center mb-2 mb-md-0">
        <p class = "m-0 p-0">${el["amount "]} $</p>
        </div>
        <div class="col-6 col-md-4 mb-2 mb-md-0">
        <p class = "m-0 p-0">Nome operazione</p>
        <p class = "m-0 p-0">${el.operation_name}</p>
        </div>
        <div class="col-6 col-md-3 mb-2 mb-md-0">
        <p class = "m-0 p-0">Categoria</p>
        <p class = "m-0 p-0">${el.operation_category}</p>
        </div>
        `
        cardBody.appendChild(row)
    });
    
    
})