let lista = [
    {
        url: "https://cdn.idealo.com/folder/Product/201490/2/201490239/s10_produktbild_gross_1/sony-playstation-5-ps5.jpg",
        titolo: "PS5",
        body: "Compra la nuova PS5"
    },
    {
        url: "https://fs-prod-cdn.nintendo-europe.com/media/images/08_content_images/systems_5/nintendo_switch_3/not_approved_1/NSwitchTop.png",
        titolo: "Switch",
        body: "Compra la nuova Switch"
    },
    {
        url: "https://m.media-amazon.com/images/I/51hFjbkxcxL._AC_SX679_.jpg",
        titolo: "Xbox",
        body: "Compra la nuova Xbox"
    },
    {
        url: "https://images.squarespace-cdn.com/content/v1/5f04f767ded4a14e876e6190/dbacf9b9-5b7a-4e64-9e4d-7a5b037c60b9/IMG_1947.jpg",
        titolo: "Miyoo Mini",
        body: "Compra la nuova Miyoo Mini"
    }
]

let paragrafo = document.querySelectorAll("p")

let nascondi = document.querySelector("#nascondi")
let colore = document.querySelector("#colore")
let grassetto = document.querySelector("#grassetto")
let magic = document.querySelector("#magic")

let randomColor = 0;

function createCard(lista) {

    let row = document.createElement("div")
    row.id = "rowCards"
    row.classList.add("row")
    row.classList.add("mb-5")
    document.getElementById('container').appendChild(row);

    let col;

    let card;

    let img;

    let title;

    let body;



    lista.forEach((item, index) => {

        col = document.createElement("div")
        col.classList.add("col-12")
        col.classList.add("col-md-3")
        col.id = "col" + (index + 1);
        document.getElementById('rowCards').appendChild(col);

        card = document.createElement("div")
        card.classList.add("card")
        card.classList.add("h-100")
        card.id = "card" + (index + 1);
        document.getElementById("col" + (index + 1)).appendChild(card);

        img = document.createElement("img")
        img.src = item.url
        img.classList.add("card-img-top")
        document.getElementById("card" + (index + 1)).appendChild(img);

        cardBody = document.createElement("div")
        cardBody.classList.add("card-body")
        cardBody.id = "cardBody" + (index + 1);
        document.getElementById("card" + (index + 1)).appendChild(cardBody);

        title = document.createElement("h5")
        title.classList.add("card-title")
        title.innerHTML = item.titolo
        document.getElementById("cardBody" + (index + 1)).appendChild(title);

        body = document.createElement("p")
        body.classList.add("card-text")
        body.innerHTML = item.body
        document.getElementById("cardBody" + (index + 1)).appendChild(body);

    })



}

nascondi.addEventListener("click", () => paragrafo.forEach((valore) => valore.classList.toggle("d-none")))

colore.addEventListener("click", () => {

    paragrafo.forEach((valore) => {

        randomColor = "#" + (Math.floor(Math.random() * 16777215).toString(16));

        if (valore.style.color != "") valore.style.color = ""
        else valore.style.color = randomColor;

    }

    )
})

grassetto.addEventListener("click", () => paragrafo.forEach((valore) => valore.classList.toggle("fw-bold")))

magic.addEventListener("click", () => {

    createCard(lista)
    magic.remove();

})

