let lista = [
    {
        url: "https://cdn.idealo.com/folder/Product/201490/2/201490239/s10_produktbild_gross_1/sony-playstation-5-ps5.jpg",
        titolo: "PS5",
        body: "Compra la nuova PS5"
    },
    {
        url: "https://fs-prod-cdn.nintendo-europe.com/media/images/08_content_images/systems_5/nintendo_switch_3/not_approved_1/NSwitchTop.png",
        titolo: "Switch",
        body: "Compra la nuova Switch"
    },
    {
        url: "https://m.media-amazon.com/images/I/51hFjbkxcxL._AC_SX679_.jpg",
        titolo: "Xbox",
        body: "Compra la nuova Xbox"
    },
    {
        url: "https://images.squarespace-cdn.com/content/v1/5f04f767ded4a14e876e6190/dbacf9b9-5b7a-4e64-9e4d-7a5b037c60b9/IMG_1947.jpg",
        titolo: "Miyoo Mini",
        body: "Compra la nuova Miyoo Mini"
    }
]

let paragrafo = document.querySelectorAll("p")

let nascondi = document.querySelector("#nascondi")
let colore = document.querySelector("#colore")
let grassetto = document.querySelector("#grassetto")

let randomColor = 0;

function fillCard(lista) {


    let cards = document.querySelectorAll(".card")

    let nomeClasse = "";
    let card;


    lista.forEach((item, index)=> {

        nomeClasse = ".card"+(index+1);

        card = document.querySelector(nomeClasse)
        card.querySelector("img").src = item.url
        card.querySelector(".card-body .card-title").innerHTML = item.titolo
        card.querySelector(".card-body .card-text").innerHTML = item.body

    })


}

fillCard(lista)

nascondi.addEventListener("click", () => {

    paragrafo.forEach((valore) => {

        if (valore.classList.contains("d-none"))
            valore.classList.remove("d-none")
        else valore.classList.add("d-none")

    }

    )
})

colore.addEventListener("click", () => {

    paragrafo.forEach((valore) => {

        randomColor = "#" + (Math.floor(Math.random() * 16777215).toString(16));

        valore.style.color = randomColor;

    }

    )
})

grassetto.addEventListener("click", () => {

    paragrafo.forEach((valore) => {

        if (valore.classList.contains("fw-bold"))
            valore.classList.remove("fw-bold")
        else valore.classList.add("fw-bold")

    }

    )
})

