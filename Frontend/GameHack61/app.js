window.onscroll = function () {
    let navbar = document.querySelector('.navbar')
    if (window.scrollY > 50) {
        navbar.classList.add('bg-black', 'shadow-lg')
        navbar.style.borderBottom = "2px solid white"
    } else {
        navbar.classList.remove('bg-black', 'shadow-lg')
        navbar.style.borderBottom = ""
    }

}
