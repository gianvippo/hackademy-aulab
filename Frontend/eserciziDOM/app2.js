let contacts = ['Nicola', 'Fabio', 'Luca', 'Giulia']
let button = document.querySelector("#firstBtn")

function createCard(val) {

    function orderAZ(val) {

        let listaArray = "";
    
        let lista = document.querySelector("ul")
        lista.innerHTML = "";
        val.sort();
        val.forEach( (valore) => {
            listaArray += `<li>${valore}</li>`
        })
        lista.innerHTML = listaArray
    
    }

    function orderZA(val) {

        let listaArray = "";
    
        let lista = document.querySelector("ul")
        lista.innerHTML = "";
        val.sort();
        val.reverse();
        val.forEach( (valore) => {
            listaArray += `<li>${valore}</li>`
        })
        lista.innerHTML = listaArray
    
    }

    let container = document.createElement("div")
    container.classList.add("container")
    document.body.appendChild(container)

    let row = document.createElement("div")
    row.classList.add("row")
    container.appendChild(row)

    let col = document.createElement("div")
    col.classList.add("col-12", "col-md-4")
    row.appendChild(col)

    let col1 = document.createElement("div")
    col1.classList.add("col-12", "col-md-8", "my-auto")
    row.appendChild(col1)

    let rowBtn = document.createElement("div")
    rowBtn.classList.add("row", "p-2")
    col1.appendChild(rowBtn)

    let rowBtn1 = document.createElement("div")
    rowBtn1.classList.add("row", "p-2")
    col1.appendChild(rowBtn1)

    let colBtn = document.createElement("div")
    colBtn.classList.add("col", "d-flex", "justify-content-center")
    rowBtn.appendChild(colBtn)

    let colBtn1 = document.createElement("div")
    colBtn1.classList.add("col", "d-flex", "justify-content-center")
    rowBtn1.appendChild(colBtn1)

    let card = document.createElement("div")
    card.classList.add("card")
    col.appendChild(card)

    let cardBody = document.createElement("div")
    cardBody.classList.add("card-body")
    card.appendChild(cardBody);

    let listaArray = "";

    let list = document.createElement("ul")
    val.forEach( (valore) => {
        listaArray += `<li>${valore}</li>`
    })
    list.innerHTML = listaArray
    cardBody.appendChild(list);

    let bottone1 = document.createElement("button")
    bottone1.type = "button"
    bottone1.classList.add("btn", "btn-warning", "p-3")
    bottone1.innerHTML = "Ordina A-Z"
    colBtn.appendChild(bottone1)

    let bottone2 = document.createElement("button")
    bottone2.type = "button"
    bottone2.classList.add("btn", "btn-primary", "p-3")
    bottone2.innerHTML = "Ordina Z-A"
    colBtn1.appendChild(bottone2)

    bottone1.addEventListener("click", () => {

        orderAZ(val)
    
    })

    bottone2.addEventListener("click", () => {

        orderZA(val)
    
    })

}

button.addEventListener("click", () => {

    createCard(contacts)
    button.disabled = true

})