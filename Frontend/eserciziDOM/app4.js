let articles = [
  {
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "breve descrizione 1",
    "image": "https://picsum.photos/288"
  },
{
    "id": 2,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "breve descrizione 2",
    "image": "https://picsum.photos/289"
  },
{
    "id": 3,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "breve descrizione 3",
    "image": "https://picsum.photos/290"
  },
{
    "id": 4,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "breve descrizione 4",
    "image": "https://picsum.photos/291"
  },
{
    "id": 5,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "breve descrizione 5",
    "image": "https://picsum.photos/292"
  }
]

let button = document.querySelector("#firstBtn")

function createGroup(val) {

    let container = document.createElement("div")
    container.classList.add("container")
    document.body.appendChild(container)

    let row = document.createElement("div")
    row.classList.add("row")
    container.appendChild(row)

    val.forEach((item) => {

        col = document.createElement("div")
        col.classList.add("col-12","col-md-3", "mb-5")
        row.appendChild(col);

        card = document.createElement("div")
        card.classList.add("card","h-100")
        col.appendChild(card);

        img = document.createElement("img")
        img.src = item.image
        img.classList.add("card-img-top")
        card.appendChild(img);

        cardBody = document.createElement("div")
        cardBody.classList.add("card-body")
        card.appendChild(cardBody);

        title = document.createElement("h4")
        title.classList.add("card-title")
        title.innerHTML = item.title
        cardBody.appendChild(title);

        body = document.createElement("p")
        body.classList.add("card-text", "lead")
        body.innerHTML = item.body
        cardBody.appendChild(body);

    })

}

button.addEventListener("click", () => {

    createGroup(articles)
    button.disabled = true

})