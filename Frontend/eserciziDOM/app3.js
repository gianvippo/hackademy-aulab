let foods = ['Pasta', 'Verdura', 'Carne', 'Frutta', 'Latticini'];
let button = document.querySelector("#firstBtn")

function createGroup(val) {

    let container = document.createElement("div")
    container.classList.add("container")
    document.body.appendChild(container)

    let row = document.createElement("div")
    row.classList.add("row")
    container.appendChild(row)

    let col = document.createElement("div")
    col.classList.add("col-12", "col-md-4")
    row.appendChild(col)

    let listaArray = "";

    let list = document.createElement("ul")
    list.classList.add("list-group")
    val.forEach( (valore) => {
        listaArray += `<li class="list-group-item">${valore}</li>`
    })
    list.innerHTML = listaArray
    col.appendChild(list);

}

button.addEventListener("click", () => {

    createGroup(foods)
    button.disabled = true

})