let titolo = document.querySelector("#input1")
let paragrafo = document.querySelector("#input2")
let button = document.querySelector(".btn")


function createArticle(tit, par) {

    const date = new Date();

    let col = document.createElement("div")
    col.classList.add("col-12", "col-md-4", "mb-3")
    document.getElementById("articoli").appendChild(col)

    card = document.createElement("div")
    card.classList.add("card")
    col.appendChild(card)

    cardBody = document.createElement("div")
    cardBody.classList.add("card-body")
    card.appendChild(cardBody);

    title = document.createElement("h2")
    title.classList.add("card-title")
    title.innerHTML = tit.value
    cardBody.appendChild(title);

    body = document.createElement("p")
    body.classList.add("card-text")
    body.innerHTML = par.value
    cardBody.appendChild(body);

    time = document.createElement("h5")
    time.innerHTML = `Pubblicato il ${date.toLocaleDateString("it-IT")}`
    cardBody.appendChild(time);

}


button.addEventListener("click", () => {

    createArticle(titolo, paragrafo)

})

/*col.innerHTML = `
    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <h5>${}
            </div>
            <div class="card-body">
            
            </div>
        </div>
    </div>` */