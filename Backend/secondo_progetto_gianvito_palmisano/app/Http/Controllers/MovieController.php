<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function viewMovie($id) {
        return view('film', ['id' => $id] );
    }

    public function addReview() {
        $movies = json_decode(file_get_contents(storage_path()."/movies.json"), true);
        return view('recensione', ['movies'=>$movies]);
    }
}
