<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function viewHomepage() {
        $movies = json_decode(file_get_contents(storage_path()."/movies.json"), true);
        return view('welcome', ['movies'=>$movies]);
    }

    public function viewAbout() {
        return view('about');
    }
}
