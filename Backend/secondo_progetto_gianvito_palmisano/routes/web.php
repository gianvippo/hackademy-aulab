<?php

use App\Http\Controllers\MovieController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'viewHomepage'])->name('home');

Route::get('/chi-siamo', [PublicController::class, 'viewAbout'])->name('about');

Route::get('/nome/{id}', [MovieController::class, 'viewMovie'])->name('film');

Route::get('/aggiungi-recensione', [MovieController::class, 'addReview'])->name('recensione');
Route::post('/aggiungi-recensione', [MovieController::class, 'metod'])->name('recensione.submit');

