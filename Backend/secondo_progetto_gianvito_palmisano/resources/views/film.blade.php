<x-layout>
    <x-header>
        <section class="container mt-5">
            <div class="row">
                <div class="col text-center">
                    <h1 class="text-white text-p">Scheda film</h1>
                </div>
            </div>

            <div class="row h-100">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col">
                            <h1 class="text-white text-p">Poster</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col poster">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col titolo">
                            <h1 class="text-white text-p">Titolo</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col genere">
                            <h1 class="text-white text-p">Genere</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col anno">
                            <h1 class="text-white text-p">Anno</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col trama">
                            <h1 class="text-white text-p">Trama</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-header>


    <script>
        let id = <?php echo(json_encode($id)); ?>;
            

                let url = `http://www.omdbapi.com/?i=${id}&apikey=13de8338`;

                fetch(url)
                    .then((response) => response.json())
                    .then((dati) => {
                        
                        let poster = document.querySelector(`.poster`)
                        let img = document.createElement('img');
                        img.classList.add("rounded-3", "shadow-lg");
                        img.style.border = "2px solid black";

                        img.src = dati["Poster"];

                        poster.appendChild(img);

                        let info;

                        let titolo = document.querySelector(".titolo")
                        info = document.createElement('h2')
                        info.innerHTML = dati["Title"]
                        titolo.appendChild(info)

                        let genere = document.querySelector(".genere")
                        info = document.createElement('h2')
                        info.innerHTML = dati["Genre"]
                        genere.appendChild(info)
                        
                        let anno = document.querySelector(".anno")
                        info = document.createElement('h2')
                        info.innerHTML = dati["Year"]
                        anno.appendChild(info)

                        let trama = document.querySelector(".trama")
                        info = document.createElement('h2')
                        info.innerHTML = dati["Plot"]
                        trama.appendChild(info)

                    })

    </script>
</x-layout>