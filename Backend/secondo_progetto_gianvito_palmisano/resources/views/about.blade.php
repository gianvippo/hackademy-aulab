<x-layout>

    <x-header>

        <div class="container h-100 m-5 text-end">
            <div class="row mt-5">
                <div class="col mt-5">
                    <h1 class="display-1 text-p p-2 ">
                        MovieHackademy
                    </h1>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col">
                    <h2 class="text-p p-2 ">
                        Siamo una squadra fortissimi
                    </h2>
                    <p class="text-s fs-4">
                        Benvenuti nel nostro blog di recensioni di film bellissimi.
                    </p>
                </div>
            </div>
        </div>

    </x-header>

</x-layout>