<x-layout>

    <x-header>
        <!-- The header content -->
        <div class="container-fluid h-100 align-items-end d-flex">
            <div class="row title">
                <div class="col text-end">
                    <h1 class="display-1 text-p p-2 d-inline border border-secondary rounded-4 shadow-lg bg-white">
                        MovieHackademy
                    </h1>
                    <h2 class="text-s fs-1 mt-5">All movies in one place.</p>
                </div>
            </div>
        </div>

    </x-header>


    <section class="container-fluid mt-5">
        <div class="row justify-content-center cards">

            @for ($i = 0; $i < 5; $i++) <div class="col-md-2">

                <div class="card h-100" id="{{$movies[$i]["id"]}}">
                    <img src="" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"></h5>
                        <p class="card-text"></p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item year">Anno: <span></span></li>
                        <li class="list-group-item genre">Generi: <span></span></li>
                        <li class="list-group-item rating">Rating: <span></span></li>
                    </ul>
                    <div class="card-body link">
                        <a href="{{route('film', ['id'=>$movies[$i]["id"]])}}" class="card-link">Vai al film</a>
                    </div>
                </div>
        </div>

        @endfor

        </div>

    </section>

    <script>
        let movies = <?php echo(json_encode($movies)); ?>;
            console.log(movies);    
            
            let array = Array.from(document.querySelectorAll('.card')).reduce( (acc, val) => {

                acc.push(val["id"]);
                return acc;

                }, []);

            array.forEach(element => {

                let url = `http://www.omdbapi.com/?i=${element}&apikey=13de8338`;

                fetch(url)
                    .then((response) => response.json())
                    .then((dati) => {
                        
                        let img = document.querySelector(`#${element} > img`)
                        img.src = dati["Poster"];

                        let title = document.querySelector(`#${element} > .card-body > .card-title`)
                        title.innerHTML = dati["Title"]

                        let description = document.querySelector(`#${element} > .card-body > .card-text`)
                        description.innerHTML = dati["Plot"]

                        let year = document.querySelector(`#${element} > .list-group > .year > span`)
                        year.innerHTML = dati["Year"]

                        let genre = document.querySelector(`#${element} > .list-group > .genre > span`)
                        genre.innerHTML = dati["Genre"]

                        let rating = document.querySelector(`#${element} > .list-group > .rating > span`)
                        rating.innerHTML = dati["Ratings"][0]["Value"]

                    })

            });
    </script>
</x-layout>