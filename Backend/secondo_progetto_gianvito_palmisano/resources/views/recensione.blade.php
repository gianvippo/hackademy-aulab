<x-layout>
    <x-header>

        <div class="container h-100 m-5 text-center">
            <div class="row mt-5">
                <div class="col-12 text-p text-white">
                    <h1>Aggiungi una recensione</h1>
                </div>
            </div>
            <div class="row mt-5 form">
                <div class="col-12 d-flex justify-content-center">
                    <select name="list" id="listMovies" class="form-select form-select-lg">
                        @foreach ($movies as $movie)
                        <option value="{{$movie["id"]}}">{{$movie["title"]}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mt-5 form">
                <div class="col-12 col-md-6 poster">
                </div>
                <div class="col-12 col-md-6">
                    <form method="POST" action="{{route('recensione.submit')}}">
                        @csrf
                        <div class="mb-3">
                            <label for="title" class="form-label"><h2>Titolo Recensione</h2></label>
                            <input name="title" type="email" class="form-control" id="title"
                                aria-describedby="emailHelp">
                        </div>
                        <div class="mb-3">
                            <label for="body" class="form-label"><h2>Recensione</h2></label>
                            <textarea name="body" class="form-control" id="body" rows="4"></textarea>
                          </div>
                        <fieldset class="rating">
                            <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5"
                                title="Awesome - 5 stars"></label>
                            <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half"
                                for="star4half" title="Pretty good - 4.5 stars"></label>
                            <input type="radio" id="star4" name="rating" value="4" /><label class="full" for="star4"
                                title="Pretty good - 4 stars"></label>
                            <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half"
                                for="star3half" title="Meh - 3.5 stars"></label>
                            <input type="radio" id="star3" name="rating" value="3" /><label class="full" for="star3"
                                title="Meh - 3 stars"></label>
                            <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half"
                                for="star2half" title="Kinda bad - 2.5 stars"></label>
                            <input type="radio" id="star2" name="rating" value="2" /><label class="full" for="star2"
                                title="Kinda bad - 2 stars"></label>
                            <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half"
                                for="star1half" title="Meh - 1.5 stars"></label>
                            <input type="radio" id="star1" name="rating" value="1" /><label class="full" for="star1"
                                title="Sucks big time - 1 star"></label>
                            <input type="radio" id="starhalf" name="rating" value="half" /><label class="half"
                                for="starhalf" title="Sucks big time - 0.5 stars"></label>
                        </fieldset><br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>

        </div>

</x-header>

    <script>
        document.querySelector("#listMovies").addEventListener('click', () => {

                let id = document.querySelector("#listMovies").value

                let url = `http://www.omdbapi.com/?i=${id}&apikey=13de8338`;

                fetch(url)
                    .then((response) => response.json())
                    .then((dati) => {
                        
                        let poster = document.querySelector(".poster")
                        poster.innerHTML = "";

                        let img = document.createElement('img')
                        img.src = dati["Poster"]
                        poster.appendChild(img)

                    })


            })

    </script>
</x-layout>