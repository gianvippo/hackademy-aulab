<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MovieHackademy</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster+Two:ital,wght@1,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web&display=swap" rel="stylesheet">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body>
    
    <nav class="navbar navbar-dark navbar-expand-md fixed-top p-md-3 p-2 pe-4">
        <a href="{{route('home')}}">
            <img src="{{asset('/icon.png')}}" alt="" class="logo p-1 bg-black rounded-circle">
        </a>
        <button class="ms-auto navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse fs-4" id="navbarCollapse">
            <div class="nav-item m-3"><a class="nav-link text-p text-white text-center" href="{{route('recensione')}}">Aggiungi una
                    recensione</a></div>
            <div class="nav-item m-3"><a class="nav-link text-p text-center text-white"
                    href="{{route('about')}}">Chi siamo</a>
            </div>

            <div class="ms-auto d-flex justify-content-center">
                <p class="nav-item ms-3 mb-0">
                    <a class="text-decoration-none text-white" href="#">
                        <i class="bi bi-person-plus fs-1"></i>
                    </a>
                </p>
                <p class="nav-item mx-3 mb-0">
                    <a class="text-decoration-none text-white" href="#">
                        <i class="bi bi-person-check fs-1"></i>
                    </a>
                </p>
            </div>
        </div>
    </nav>

    {{ $slot }}

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
        crossorigin="anonymous">
    </script>

</body>

</html>