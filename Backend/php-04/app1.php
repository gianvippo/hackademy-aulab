<?php 

abstract class RAM{

    abstract public function read();
    abstract public function write($dati);
    abstract public function returnDati();

}

abstract class GPU{

    abstract public function input($dati);
    abstract public function render();
    abstract public function output();

}

abstract class SSD{

    abstract public function read();
    abstract public function write();
    abstract public function showDimensions();
    abstract public function returnDati();

}

abstract class CPU{

    abstract public function input($dati);
    abstract public function elaborate();
    abstract public function output();
    abstract public function returnDati();

}


class Corsair extends RAM {

    public $nome = "Corsair";
    public $dati;
    public $frequenza;

    public function read()
    {       
        echo "Lettura dati (".$this->dati.")...\n";
        $this->dati = $this->dati/8; 
    }

    public function write($dati){
        $this->dati = $dati;
        echo "Scrittura e compressione dati in corso...\n";
    }

    public function returnDati(){
        return $this->dati;
    }

    public function __construct($frequenza)
    {
        $this->frequenza = $frequenza;
    }

}

class Kingston extends RAM {
    
    public $nome = "Kingston";
    public $dati;
    public $frequenza = 4000;

    public function read()
    {       
        echo "Lettura dati (".$this->dati.")...\n";
        $this->dati = $this->dati/8; 
    }

    public function write($dati){
        $this->dati = $dati;
        echo "Scrittura e compressione dati in corso...\n";
    }

    public function returnDati(){
        return $this->dati;
    }


}

class Nvidia extends GPU {

    public $nome;
    public $dati;
    public $render;

    public function input($dati) {
        $this->dati = $dati;
        echo "Lettura dati...\n";
    }

    public function render() {
        echo "Rendering...\n";
        $this->render = "#########################\n#\t\t\t#\n#\t ".$this->dati."\t\t#\n#\t\t\t#\n#########################\n";
    }

    public function output() {
        echo "Invio output...\n";
        echo $this->render;
    }

    public function __construct($nome)
    {
        $this->nome = $nome;
    }
}

class SanDisk extends SSD {

    public $name = "SanDisk";
    public $dati;
    public $size;

    public function showDimensions(){
        echo "La dimensione dell'SSD SanDisk è $this->size TB.\n";
    }

    public function read() {
        echo "Lettura in corso...\n";
    }

    public function returnDati(){
        return $this->dati;
    }

    public function write() {    
        $this->dati = readline("Inserisci un dato nell'SSD: ");
        echo "\nScrittura in corso...\n";
    }

    public function __construct($size)
    {
        $this->size = $size;
    }

}

class AMD extends CPU {

    public $nome;
    public $dati;

    public function input($dati) {
        echo "Lettura input...\n";
        echo "Dato inserito $dati\n";
        $this->dati = $dati;
    }

    public function elaborate() {
        $this->dati++;
        echo "Elaborazione...\n";
    }
    public function output() {
        echo "Invio dato...\n";
        echo "Dato elaborato: $this->dati\n";
    }

    public function returnDati() {
        return $this->dati;
    }

    public function __construct($nome)
    {
        $this->nome = $nome;
    }
}

class ASUS{

    public $BIOS_ver = 5.1;
    public $RAM;
    public $GPU;
    public $CPU;
    public $SSD;
    public $dati;

    public function __construct(RAM $memoria, GPU $video, SSD $allocazione, CPU $procio)
    {
        $this->RAM = $memoria;
        $this->GPU = $video;
        $this->CPU = $procio;
        $this->SSD = $allocazione;
    }

    public function mostraDimensioni(){
        $this->SSD->showDimensions();
    }

    public function letturaRAM(){
        $this->RAM->read();
    }

    public function scritturaRAM($dati){
        $this->RAM->write($dati);
    }

    public function inputGPU($dati){
        $this->GPU->input($dati);
    }

    public function outputGPU(){
        $this->GPU->output();
    }

    public function renderGPU(){
        $this->GPU->render();
    }

    public function inputCPU($dati){
        $this->CPU->input($dati);
    }

    public function elaborazioneCPU(){
        $this->CPU->elaborate();
    }

    public function outputCPU(){
        $this->CPU->output();
    }

    public function letturaSSD(){
        $this->SSD->read();
    }

    public function scritturaSSD(){
        $this->SSD->write();
    }
}

$scheda_madre = new ASUS(new Corsair(3200), new Nvidia("Nvidia 3080 Ti"), new SanDisk(2), new AMD("Ryzen 7"));

print_r($scheda_madre);

$scheda_madre->mostraDimensioni();
$scheda_madre->scritturaSSD();
$scheda_madre->letturaSSD();
$scheda_madre->inputCPU($scheda_madre->SSD->returnDati());
$scheda_madre->elaborazioneCPU();
$scheda_madre->outputCPU();
$scheda_madre->scritturaRAM($scheda_madre->CPU->returnDati());
$scheda_madre->letturaRAM();
$scheda_madre->inputCPU($scheda_madre->RAM->returnDati());
$scheda_madre->elaborazioneCPU();
$scheda_madre->outputCPU();
$scheda_madre->inputGPU($scheda_madre->CPU->returnDati());
$scheda_madre->renderGPU();
$scheda_madre->outputGPU();