<?php

namespace App\Http\Controllers;

use Exception;
use App\Mail\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function viewHome()
    {
        return view('homepage');
    }

    public function viewAbout()
    {
        $users = User::all();
        return view('about', compact('users'));
    }

    public function viewContact()
    {
        return view('contact');
    }

    public function submitContact(Request $request)
    {

        try {

            $name = $request->name;
            $email = $request->email;
            $phone = $request->phone;
            $message = $request->message;


            $data = compact('name','email','phone','message');

            Mail::to('info@nationalpark.com')->send(new ContactForm($data));

            return redirect(route('homepage'))->with('success', 'Messaggio inviato!');
        } catch (Exception $error) {
            return redirect(route('homepage'))->with('errorMessage', `Qualcosa è andato storto, riprova!\nErrore $error`);
        }
    }
}
