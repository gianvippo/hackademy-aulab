<x-layout title="Chi siamo | National Park">

    <section class="about-section text-center" id="about">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-8">
                    <h2 class="text-white mb-4">Chi siamo</h2>
                    <p class="text-white-50">
                        Scopri chi fa parte della nostra community
                    </p>
                </div>
            </div>
        </div>
    </section>

    <div class="container mt-5">
        <div class="row text-center">

            @foreach ($users as $user)
            
            <div class="col-xl-3 col-sm-6 mb-5">
                <div class="bg-white rounded shadow-sm py-5 px-4"><img src="{{Storage::url('public/img/default-user-profile-picture.png')}}" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">
                    <h5 class="mb-0">{{$user->name}}</h5><span class="small text-muted">CEO - Founder</span>
                    <span class="small d-block">{{$user->email}}</span>
                    <ul class="social mb-0 list-inline mt-3">
                        <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-instagram"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div><!-- End -->
            @endforeach
        </div>
    </div>

</x-layout>