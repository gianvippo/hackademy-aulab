<x-layout title="Registrati | National Park">

    
    <div class="login-dark">
        <form method="POST" action="{{route('register')}}">
            @csrf

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="illustration"><i class="fa-solid fa-user-plus"></i></div>
            <div class="form-group"><input class="form-control" type="name" name="name" id="name" placeholder="Nome e cognome"></div>
            <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" id="password" placeholder="Password"></div>
            <div class="form-group"><input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Conferma password"></div>
            <div class="form-group mt-5"><button class="btn btn-dark mx-auto d-block" type="submit">Registrati</button></div>
        </form>
    </div>
    


</x-layout>