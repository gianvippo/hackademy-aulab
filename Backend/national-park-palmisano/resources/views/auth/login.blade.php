<x-layout title="Login | National Park">

    
        <div class="login-dark">
            <form method="POST" action="{{route('login')}}">
                @csrf
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="illustration"><i class="fa-regular fa-user"></i></div>
                <div class="form-group"><input class="form-control" type="email" name="email" id="email" placeholder="Email"></div>
                <div class="form-group"><input class="form-control" type="password" name="password" id="password" placeholder="Password"></div>
                <fieldset class="form-group">
                    <div class="form-check mt-3">
                        <input class="form-check-input" type="checkbox" value="" id="remember"
                            checked="">
                        <label class="form-check-label" for="remember">
                            Ricordami
                        </label>
                    </div>
                </fieldset>
                <div class="form-group mt-4"><button class="btn btn-dark mx-auto d-block" type="submit">Log In</button></div>
            </form>
        </div>
        


</x-layout>