<x-layout title="Contattaci | National Park">

    @vite('resources/css/contact.css')

    <section class="about-section text-center" id="about">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-8">
                    <h2 class="text-white mb-4">Contattaci</h2>
                    <p class="text-white-50">
                        Saremo lieti di risponderti il prima possibile
                    </p>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <form action="{{route('submit.contact')}}" method="POST" enctype="multipart/form-data">
        <div class="row input-container">
                @csrf
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="col-xs-12">
                    <div class="styled-input wide">
                        <input type="name" id="name" name="name" required />
                        <label>Nome</label>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="styled-input">
                        <input type="email" id="email" name="email" required />
                        <label>Email</label>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="styled-input" style="float:right;">
                        <input type="number" id="phone" name="phone" required />
                        <label>Numero di telefono</label>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="styled-input wide">
                        <textarea name="message" id="message" required></textarea>
                        <label>Messaggio</label>
                    </div>
                </div>
                <div class="form-group mt-3"><button class="btn btn-dark mx-auto d-block" type="submit">Invia</button>
                </div>
            </div>
        </form>
    </div>

</x-layout>