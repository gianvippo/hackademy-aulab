<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nuovo messaggio da {{$name}}</title>
</head>
<body>
    
    <h1>Ciao, {{$name}} ti ha scritto</h1>
    <br><br>
    <h2>Numero di telefono: {{$phone}} <br>Email: {{$email}}<br>Testo: <br></h2>
    <h3>{{$text}}</h3>
</body>
</html>