<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="{{route('homepage')}}">National Park</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item"><a class="nav-link" href="{{route('about')}}">Chi siamo</a></li>
                <li class="nav-item"><a class="nav-link" href="#projects">Blog</a></li>
                <li class="nav-item"><a class="nav-link" href="{{route('contact')}}">Contattaci</a></li>
            </ul>
        </div>
        @if (Auth::guest())
        <ul class="navbar-nav">
          <li class="nav-item" title="Login">
            <a class="nav-link mx-auto" href="{{route('login')}}">
              <i class="fa-solid fa-right-to-bracket fs-4"></i>
            </a>
          </li>
          <li class="nav-item" title="Registrati">
            <a class="nav-link" href="{{route('register')}}">
              <i class="fa-solid fa-user-plus fs-4"></i>
            </a>
          </li>
        </ul>
        @else
        <ul class="navbar-nav">
          <li class="nav-item my-auto">
              <p class="my-auto">Bentornato {{Auth::user()->name}}</p>
          </li>
          <li class="nav-item" title="Logout">
            <a class="nav-link" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();" href="{{route('logout')}}">
              <form id='form-logout' method='POST' action='{{route('logout')}}' class='d-none'>@csrf</form>
              <i class="fa-solid fa-right-from-bracket fs-4"></i>
            </a>
          </li>
        </ul>           
        @endif
    </div>
</nav>