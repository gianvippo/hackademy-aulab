<?php

$cont = 0;

function checkPass($string) {

    $upperCase = false;
    $lowerCase = false;
    $num = false;
    $specialChar = false;

    if(strlen($string) < 9) {
        echo "\nLa password è minore di 8 caratteri.\n\n"; 
        return false;
    }

    for($i = 0; $i<strlen($string); $i++) {

        if(ctype_upper($string[$i])) $upperCase = true;
        if(ctype_lower($string[$i])) $lowerCase = true;
        if(is_numeric($string[$i])) $num = true;
        if(preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $string)) $specialChar = true;

    }

    if($upperCase && $lowerCase && $num && $specialChar) return true;
    else {
        if(!$upperCase) echo "\n [*] La password non contiene caratteri maiuscoli.\n";
        if(!$lowerCase) echo "\n [*] La password non contiene caratteri minuscoli.\n";
        if(!$num) echo "\n [*] La password non contiene numeri.\n";
        if(!$specialChar) echo "\n [*] La password non contiene caratteri speciali.\n";

        echo "\n";

        return false;
    }
}

echo "\nLa password dovrà essere di minimo 8 caratteri e dovrà contenere:\n [*] Un carattere maiuscolo.\n [*] Un carattere minuscolo.\n [*] Un numero.\n [*] Un carattere speciale.\n";

while($cont < 4) {

    $password = readline("Inserire la password: ");
    $cont++;

    if (checkPass($password)) { 
        $cont = 4;
        echo "La password inserita è corretta.";

    } elseif($cont < 4) {
        echo "Tentativo numero $cont.\n\n";
    } elseif($cont == 4) {
        echo "Tentativi massimi raggiunti.";
    }

}