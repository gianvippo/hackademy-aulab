<x-layout document="Aggiungi Film | laravelDB">

    <x-header name="Aggiungi un film al DB" />

    <div class="container vh-min-100">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <form method="POST" action="{{route('add.movie.submit')}}">
                    @csrf
                    <!-- Name input -->

                    <div class="form-outline mb-4">
                      <input name="title" type="text" id="title" class="form-control" required/>
                      <label class="form-label" for="title">Titolo</label>
                    </div>
                  
                    <!-- Email input -->
                    <div class="form-outline mb-4">
                      <input name="year" type="number" min="1900" max="2022" step="1" id="year" class="form-control" required/>
                      <label class="form-label" for="year">Anno</label>
                    </div>
                  
                    <!-- Message input -->
                    <div class="form-outline mb-4">
                      <textarea name="plot" class="form-control" id="plot" rows="4" required></textarea>
                      <label class="form-label" for="plot">Trama</label>
                    </div>
                  
                    <!-- Checkbox -->
                    <div class="form-check d-flex justify-content-center mb-4">
                      <input class="form-check-input me-2" name="checkbox" type="checkbox" value="true" id="checkbox" checked />
                      <label class="form-check-label" for="checkbox">
                        Inviami una copia via mail
                      </label>
                    </div>

                    <div class="form-outline mb-4">
                        <input name="email" type="email" id="email" class="form-control" />
                        <label class="form-label" for="email">Indirizzo Email</label>
                      </div>
                  
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-block mb-4 mx-auto d-block">Invia</button>
                  </form>
            </div>
        </div>
    </div>


</x-layout>