<x-layout document="Benvenuti | laravelDB">

    <x-header name='Benvenuti' />

    <div class="container">
        <div class="row">
            <div class="col">
                <h2>
                    Ciao a tutti
                </h2>
            </div>
            @if (session('success'))
            <div class="row">
                <div class="col">
                    <h3 class="alert alert-success">{{session('success')}}</h3>
                </div>
            </div>
            @endif
            @if (session('danger'))
            <div class="row">
                <div class="col">
                    <h3 class="alert alert-danger">{{session('danger')}}</h3>
                </div>
            </div>
            @endif

        </div>
    </div>




</x-layout>