<div class="container my-5">
    <div class="row">
        <div class="col">
            <h1 class="display-2 text-center fst-italic">
                {{$name}}
            </h1>
        </div>
    </div>
</div>