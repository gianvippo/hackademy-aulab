<x-layout document="Tutti i Film | laravelDB">

    <x-header name='Tutti i Film' />

    <div class="container vh-min-100">
        <div class="row">
            @foreach ($films as $film)
            <div class="col-12 col-md-3 m-3">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="https://picsum.photos/300" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Titolo: {{$film->title}}</h5>
                        <p class="text-muted fst-italic">Anno: {{$film->year}}</p>
                        <p class="card-text">Trama: {{$film->plot}}</p>
                        <a href="#" class="btn btn-primary">Scopri di più</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>


</x-layout>