<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Film aggiunto correttamente!</title>
</head>
<body>
    
    <h1>Grazie per avere aggiunto un nuovo film!</h1>

    <h2>Nome del film: {{$title}}</h2>
    <h2>Anno: {{$year}}</h2>
    <h3>Trama: {{$plot}}</h2>



</body>
</html>