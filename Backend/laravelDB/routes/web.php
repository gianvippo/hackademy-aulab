<?php

use App\Http\Controllers\MovieController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'viewHome'])->name('homepage');

Route::get('/index-movie', [MovieController::class, 'viewAllMovie'])->name('index.movie');

Route::get('/add-movie', [MovieController::class, 'addMovie'])->name('add.movie');
Route::post('/add-movie/submit', [MovieController::class, 'submitMovie'])->name('add.movie.submit');