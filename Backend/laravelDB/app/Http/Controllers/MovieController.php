<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmationMail;
use App\Models\Film;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MovieController extends Controller
{
    public function viewAllMovie()
    {
        $dati = Film::all();
        return view('index', ['films'=>$dati]);
    }

    public function addMovie()
    {
        return view('addMovie');
    }

    public function submitMovie(Request $request)
    {
        $title = $request->title;
        $year = $request->year;
        $plot = $request->plot;

        $film = new Film();
        $film->title = $title;
        $film->year = $year;
        $film->plot = $plot;

        $data = compact('title', 'year', 'plot');

        $check = $request->checkbox;

        if ($check && isset($request->email)) {

            try {
                $film->save();
                Mail::to($request->email)->send(new ConfirmationMail($data));
                return redirect(route('homepage'))->with('success', 'Film aggiunto e mail inviata correttamente!');

            } catch (Exception $error) {
                return redirect(route('homepage'))->with('danger', 'Qualcosa è andato storto, riprova!');
            }

        } else {
            $film->save();
            return redirect(route('homepage'))->with('success', 'Film aggiunto correttamente!');
        }
    }
}
