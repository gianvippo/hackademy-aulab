<?php

$array = [];
$check = 1;

while ($check) {

    $numbers = readline("\nInserisci il numero di utenti da inserire: ");

    if (is_numeric($numbers)) {

        $id = 1;

        for ($i = 0; $i < $numbers; $i++) {

            $cont = 1;

            while ($cont) {
                $input = readline("\nInserisci il nome dell'utente $id: ");
                if (ctype_alpha($input)) {

                    $array[$i]["name"] = ucfirst($input);
                    $cont--;
                } else echo "\n!!! - Non hai scritto un nome valido, riscrivi - !!!\n\n";
            }

            $cont = 1;

            while ($cont) {
                $input = readline("\nInserisci il cognome dell'utente $id: ");
                if (ctype_alpha($input)) {

                    $array[$i]["surname"] = ucfirst($input);
                    $cont--;
                } else echo "\n!!! - Non hai scritto un cognome valido, riscrivi - !!!\n\n";
            }

            $cont = 1;

            while ($cont) {
                $input = readline("\nInserisci il genere dell'utente $id: ");
                if (strcasecmp($input, "maschio") == 0 || strcasecmp($input, "uomo") == 0) {

                    $array[$i]["gender"] = "uomo";
                    $cont--;

                } elseif(strcasecmp($input, "femmina") == 0 || strcasecmp($input, "donna") == 0) {

                    $array[$i]["gender"] = "donna";
                    $cont--;

                } else echo "\n!!! - Non hai scritto un genere valido, riscrivi - !!!\n\n";
            }

            $id++;
        }

        $check--;
    } else echo "!!! - Non hai scritto un numero, riscrivi - !!!\n\n";
}

foreach ($array as $person) {
    if($person["gender"]=="donna") echo "\nBuongiorno Sig.ra " . $person["name"] . " " . $person["surname"] . "!";
    if($person["gender"]=="uomo") echo "\nBuongiorno Sig. " . $person["name"] . " " . $person["surname"] . "!";
}

?>