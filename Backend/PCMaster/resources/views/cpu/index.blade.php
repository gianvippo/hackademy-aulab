<x-layout title="CPU | PCMaster">

    <x-header>
        <h1 class="page-title">Elenco CPU</h1>
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row">

                    @foreach ($cpus as $cpu)
                    <div class="div col-12 col-md-3">
                        
                        <div class="card mb-3">
                            <h3 class="card-header">{{$cpu->name}}</h3>
                            <div class="card-body">
                              <h4 class="card-title">Marca: {{$cpu->brand}}</h4>
                            </div>
                            <img class="mx-auto rounded-1" src="{{$cpu->img ? Storage::url($cpu->img) : Storage::url('public/cpu/default.jpg')}}" alt="Immagine CPU" width="200px">
                            <div class="card-body">
                              <a href="{{route('cpu.show', ['cpu' => $cpu, 'name' => $cpu->name])}}" class="card-link">Scopri di più</a>
                            </div>
                            <div class="card-footer text-muted">
                              Pubblicato il: {{date('d/m/Y H:i', strtotime($cpu->updated_at))}}
                            </div>
                          </div>
                    </div>
                    
                    @endforeach

                </div>
            </div>
        </div>
    </div>

</x-layout>