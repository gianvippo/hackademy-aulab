<x-layout title="CPU | PCMaster">

    <x-header>
        <h1 class="page-title">Dettaglio CPU</h1>
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row">

                    <div class="div col-12 col-md-3">
                        
                        <div class="card mb-3">
                            <h3 class="card-header">{{$cpu->name}}</h3>
                            <div class="card-body">
                              <h4 class="card-title">Marca: {{$cpu->brand}}</h4>
                            </div>
                            <img class="mx-auto rounded-1" src="{{$cpu->img ? Storage::url($cpu->img) : Storage::url('public/cpu/default.jpg')}}" alt="Immagine CPU" width="200px">
                            <ul class="list-group list-group-flush">
                              <li class="list-group-item">Frequenza: {{$cpu->frequency}} MHz</li>
                              <li class="list-group-item">Threads: {{$cpu->threads}}</li>
                              <li class="list-group-item">TDP: {{$cpu->tdp}} W</li>
                              <li class="list-group-item">Inserito da: {{$cpu->user->name}}</li>
                            </ul>
                            <div class="card-body">
                              <a href="@if(url()->previous() == url()->current())
                                {{route('homepage')}}
                                @else 
                                {{url()->previous()}}
                                @endif" class="card-link">Torna indietro</a>
                            </div>
                            @if (isset(Auth::user()->id))
                            @if($cpu->user_id == Auth::user()->id)
                            <div class="card-body d-flex justify-content-around">
                              <a href="{{route('cpu.edit', ['cpu' => $cpu, 'name' => $cpu->name])}}" class="card-link">Modifica</a>
                              <form method="POST" id="form" action="{{route('cpu.delete', ['cpu' => $cpu, 'name' => $cpu->name])}}" class="d-inline">
                                @csrf
                                @method('delete')
                                <a href="#" class="card-link" onclick="document.querySelector('#form').submit();">Elimina</a>
                              </form>
                            </div>
                            @endif
                            @endif
                            <div class="card-footer text-muted">
                              Pubblicato il: {{date('d/m/Y H:i', strtotime($cpu->updated_at))}}
                            </div>
                          </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</x-layout>