<x-layout title="Modifica CPU | PCMaster">

    <x-header>
        <h1 class="page-title">Modifica CPU</h1>
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row justify-content-center">

                    <form class="col-12 col-md-6" action="{{route('cpu.update', ['cpu' => $cpu, 'name' => $cpu->name])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        
                        <fieldset>
                            <div class="brand-group">
                                <label for="brand" class="form-label mt-4">Brand</label>
                                <select name="brand" class="form-select" id="brand">
                                    <option value="{{$cpu->brand}}">{{ucfirst($cpu->brand)}}</option>
                                    @switch($cpu->brand)
                                    @case("intel")
                                    <option value="amd">AMD</option>
                                    @break
                                    @case("amd")
                                    <option value="intel">Intel</option>
                                    @break
                                    @default
                                    <option value="intel">Intel</option>
                                    <option value="amd">AMD</option>
                                    @endswitch
                                </select>
                            </div>
                            <div id="model-form">
                                <label for="name" class="form-label mt-4">Modello</label>
                                <select name="name" class="form-select" id="name">
                                    <option value="{{$cpu->name}}">{{$cpu->name}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="frequency" class="form-label mt-4">Frequenza</label>
                                <input name="frequency" type="number" class="form-control" id="frequency"
                                    placeholder="Inserisci la frequenza in MHz" min="100" max="5000" step="100" value="{{$cpu->frequency}}">
                            </div>
                            <div class="form-group">
                                <label for="cores" class="form-label mt-4">Cores</label>
                                <input name="cores" type="number" class="form-control" id="cores"
                                    placeholder="Inserisci il numero di core" min="2" max="64" step="2" value="{{$cpu->cores}}">
                            </div>
                            <div class="form-group">
                                <label for="threads" class="form-label mt-4">Threads</label>
                                <input name="threads" type="number" class="form-control" id="threads"
                                    placeholder="Inserisci il numero di thread" max="64" step="2" value="{{$cpu->threads}}">
                            </div>
                            <div class="form-group">
                                <label for="tdp" class="form-label mt-4">TDP</label>
                                <input name="tdp" type="number" class="form-control" id="tdp"
                                    placeholder="Inserisci il TDP in Watt" min="1" value="{{$cpu->tdp}}">
                            </div>
                            <div class="form-group">
                                <label for="formFile" class="form-label mt-4">Inserisci l'immagine</label>
                                <input name="formFile" class="form-control" type="file" id="formFile">
                            </div>
                            <button type="submit" class="btn btn-primary mx-auto d-block mt-5">Invia</button>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>

</x-layout>

<script>
    let intelCPU = ["i3-13100", "i5-13400", "i7-13700K", "i9-13900K"]
    let amdCPU = ["Ryzen 3 7200", "Ryzen 5 7600X", "Ryzen 7 7700X", "Ryzen 9 7900X"]
    let brand = document.querySelector('#brand')
    let cores = document.querySelector('#cores')

    function createList(list) {
        let model = document.querySelector('#name')
        model.removeAttribute("disabled")
        model.innerHTML = "";
        list.forEach(element => {
            let option = document.createElement('option')
            option.value = element
            option.innerHTML = element
            model.appendChild(option)
        });
    }

    brand.addEventListener("click", () => {

        switch (brand.value) {
            case 'intel':
                createList(intelCPU)
                break;

            case 'amd':
                createList(amdCPU)
                break;

            default:
                let model = document.querySelector('#name')
                model.setAttribute("disabled", "")
                model.innerHTML = ""
                let option = document.createElement('option')
                model.appendChild(option)
                break;
        }

    })

    cores.addEventListener('click', () => {
        let thread = document.querySelector('#threads')
        thread.min = cores.value
    })
</script>