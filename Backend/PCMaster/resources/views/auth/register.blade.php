<x-layout title="Registrati | PCMaster">

    <x-header>
        <h1 class="page-title">Registrati</h1>
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6">
                        <form method="POST" action="{{route('register')}}">
                            @csrf

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="form-group">
                                <label for="name" class="form-label mt-4">Nome e Cognome</label>
                                <input name="name" type="text" class="form-control" id="name" value="{{old('name')}}"
                                    placeholder="Inserisci Nome e Cognome">
                            </div>
                            <div class="form-group">
                                <label for="email" class="form-label mt-4">Indirizzo Email</label>
                                <input name="email" type="email" class="form-control" id="email" value="{{old('name')}}"
                                    aria-describedby="emailHelp" placeholder="Inserisci un indirizzo Email valido">
                                <small id="emailHelp" class="form-text text-muted">Non condivideremo con nessuno la tua
                                    mail</small>
                            </div>
                            <div class="form-group">
                                <label for="password" class="form-label mt-4">Password</label>
                                <input name="password" type="password" class="form-control" id="password"
                                    placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation" class="form-label mt-4">Conferma Password</label>
                                <input name="password_confirmation" type="password" class="form-control"
                                    id="password_confirmation" placeholder="Ripeti la Password">
                            </div>
                            <button type="submit" class="btn btn-primary mt-3 mx-auto d-block">Registrati</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-layout>