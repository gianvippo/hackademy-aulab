<x-layout title="Login | PCMaster">

    <x-header>
        <h1 class="page-title">Esegui l'accesso</h1>
        @if (session('error'))
            <div class="div aler alert-danger fs-2 p-2 text-white rounded mt-4">
                {{session('error')}}
            </div>
        @endif
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-6">
                        <form method="POST" action="{{route('login')}}">
                            @csrf
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="email" class="form-label mt-4">Indirizzo Email</label>
                                <input name="email" type="email" class="form-control" id="email"
                                    aria-describedby="emailHelp" placeholder="Inserisci un indirizzo email valido">
                            </div>
                            <div class="form-group">
                                <label for="password" class="form-label mt-4">Password</label>
                                <input name="password" type="password" class="form-control" id="password"
                                    placeholder="Password">
                            </div>
                            <fieldset class="form-group">
                                <div class="form-check mt-3">
                                    <input class="form-check-input" type="checkbox" value="" id="remember"
                                        checked="">
                                    <label class="form-check-label" for="remember">
                                        Ricordami
                                    </label>
                                </div>
                            </fieldset>
                            <button type="submit" class="btn btn-primary mt-3 mx-auto d-block">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-layout>