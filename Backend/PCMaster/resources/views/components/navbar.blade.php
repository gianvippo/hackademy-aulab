<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('homepage')}}">PCMaster</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav me-auto">
          <li class="nav-item">
            <a class="nav-link active" href="{{route('homepage')}}">Home
              <span class="visually-hidden">(current)</span>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">GPU</a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{route('gpu.index')}}">Lista GPU</a>
              <a class="dropdown-item" href="{{route('gpu.create')}}">Aggiungi GPU</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">CPU</a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="{{route('cpu.index')}}">Lista CPU</a>
              <a class="dropdown-item" href="{{route('cpu.create')}}">Aggiungi CPU</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Separated link</a>
            </div>
          </li>
        </ul>
        @if (Auth::guest())
        <ul class="navbar-nav">
          <li class="nav-item" title="Login">
            <a class="nav-link mx-auto" href="{{route('login')}}">
              <i class="fa-solid fa-right-to-bracket fs-4"></i>
            </a>
          </li>
          <li class="nav-item" title="Registrati">
            <a class="nav-link" href="{{route('register')}}">
              <i class="fa-solid fa-user-plus fs-4"></i>
            </a>
          </li>
        </ul>
        @else
        <ul class="navbar-nav">
          <li class="nav-item my-auto me-3">
              <p class="my-auto">Bentornato {{Auth::user()->name}}</p>
          </li>
          <li class="nav-item" title="Logout">
            <a class="nav-link mx-auto" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();" href="{{route('logout')}}">
              <form id='form-logout' method='POST' action='{{route('logout')}}' class='d-none'>@csrf</form>
              <i class="fa-solid fa-right-from-bracket fs-4"></i>
            </a>
          </li>
        </ul>           
        @endif
      </div>
    </div>
  </nav>