<x-layout title="Homepage | PCMaster">

    <x-header>
        <h1 class="page-title">Benvenuti su PCMaster!</h1>
        <h2>Qui troverete i migliori componenti per PC</h2>
        @if (session('success'))
            <div class="div aler alert-success fs-2 p-2 text-white rounded mt-4">
                {{session('success')}}
            </div>
        @endif
        @if (session('errorMes'))
            <div class="div aler alert-danger fs-2 p-2 text-white rounded mt-4">
                {{session('errorMes')}}
            </div>
        @endif
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row">

                    

                </div>
            </div>
        </div>
    </div>

</x-layout>