<x-layout title="GPU | PCMaster">

    <x-header>
        <h1 class="page-title">Dettaglio GPU</h1>
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row">

                    <div class="div col-12 col-md-3">
                        
                        <div class="card mb-3">
                            <h3 class="card-header">{{$gpu->model}}</h3>
                            <div class="card-body">
                              <h4 class="card-title">Marca: {{$gpu->brand}}</h4>
                            </div>
                            <img class="mx-auto rounded-1" src="{{$gpu->img ? Storage::url($gpu->img) : Storage::url('public/gpu/default.jpg')}}" alt="Immagine GPU" width="200px">
                            <div class="card-body">
                              <p class="card-text">Descrizione: {{$gpu->description ?? "Nessuna descrizione fornita"}}</p>
                            </div>
                            <ul class="list-group list-group-flush">
                              <li class="list-group-item">Produttore: {{$gpu->vendor}}</li>
                              <li class="list-group-item">Inserito da: {{$gpu->user->name}}</li>
                            </ul>
                            <div class="card-body">
                              <a href="@if(url()->previous() == url()->current())
                                {{route('homepage')}}
                                @else 
                                {{url()->previous()}}
                                @endif" class="card-link">Torna indietro</a>
                            </div>
                            @if (isset(Auth::user()->id))
                            @if($gpu->user_id == Auth::user()->id)
                            <div class="card-body d-flex justify-content-around">
                              <a href="{{route('gpu.edit', ['gpu' => $gpu, 'model' => $gpu->model])}}" class="card-link">Modifica</a>
                              <form method="POST" id="form" action="{{route('gpu.delete', ['gpu' => $gpu, 'model' => $gpu->model])}}" class="d-inline">
                                @csrf
                                @method('delete')
                                <a href="#" class="card-link" onclick="document.querySelector('#form').submit();">Elimina</a>
                              </form>
                            </div>
                            @endif
                            @endif
                            <div class="card-footer text-muted">
                              Pubblicato il: {{date('d/m/Y H:i', strtotime($gpu->updated_at))}}
                            </div>
                          </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</x-layout>