<x-layout title="Aggiungi GPU | PCMaster">

    <x-header>
        <h1 class="page-title">Modifica GPU</h1>
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row justify-content-center">

                    <form class="col-12 col-md-6"
                        action="{{route('gpu.update', ['gpu' => $gpu, 'model' => $gpu->model])}}" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <fieldset>
                            <div class="brand-group">
                                <label for="brand" class="form-label mt-4">Brand</label>
                                <select name="brand" class="form-select" id="brand">
                                    <option value="{{$gpu->brand}}">{{strtoupper($gpu->brand)}}</option>
                                    @switch($gpu->brand)
                                    @case("nvidia")
                                    <option value="amd">AMD</option>
                                    @break
                                    @case("amd")
                                    <option value="nvidia">NVIDIA</option>
                                    @break
                                    @default
                                    <option value="nvidia">NVIDIA</option>
                                    <option value="amd">AMD</option>
                                    @endswitch
                                </select>
                            </div>
                            <div id="model-form">
                                <label for="model" class="form-label mt-4">Modello</label>
                                <select name="model" class="form-select" id="model">
                                    <option value="{{$gpu->model}}">{{$gpu->model}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="vendor" class="form-label mt-4">Produttore</label>
                                <input name="vendor" type="text" class="form-control" id="vendor"
                                    value="{{$gpu->vendor}}" placeholder="Inserisci il Produttore">
                            </div>
                            <div class="form-group">
                                <label for="formFile" class="form-label mt-4">Inserisci l'immagine</label>
                                <input name="formFile" class="form-control" type="file" id="formFile">
                            </div>
                            <div class="form-group">
                                <label for="description" class="form-label mt-4">Descrizione</label>
                                <textarea name="description" class="form-control" id="description" rows="3" value="{{$gpu->description}}"></textarea>
                              </div>
                            <button type="submit" class="btn btn-primary mx-auto d-block mt-5">Invia</button>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>

</x-layout>

<script>
    let nvidiaGPU = ["3060", "3060-ti", "3070-ti", "3080", "3090"]
    let amdGPU = ["6600", "6600-xt", "6700", "6700-xt", "6800"]
    let brand = document.querySelector('#brand')

    function createList(list) {
        let model = document.querySelector('#model')
        model.removeAttribute("disabled")
        model.innerHTML = "";
        list.forEach(element => {
            let option = document.createElement('option')
            option.value = element
            option.innerHTML = element
            model.appendChild(option)
        });
    }

    brand.addEventListener("click", () => {

        switch (brand.value) {
            case 'nvidia':
                createList(nvidiaGPU)
                break;

            case 'amd':
                createList(amdGPU)
                break;

            default:
                let model = document.querySelector('#model')
                model.setAttribute("disabled", "")
                model.innerHTML = ""
                let option = document.createElement('option')
                model.appendChild(option)
                break;
        }

    })
</script>