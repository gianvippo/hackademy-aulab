<x-layout title="GPU | PCMaster">

    <x-header>
        <h1 class="page-title">Elenco GPU</h1>
    </x-header>

    <div class="card-section">
        <div class="container">
            <div class="card-block bg-white mb30">
                <div class="row">

                    @foreach ($gpus as $gpu)
                    <div class="div col-12 col-md-3">
                        
                        <div class="card mb-3">
                            <h3 class="card-header">{{$gpu->model}}</h3>
                            <div class="card-body">
                              <h4 class="card-title">Marca: {{$gpu->brand}}</h4>
                            </div>
                            <img class="mx-auto rounded-1" src="{{$gpu->img ? Storage::url($gpu->img) : Storage::url('public/gpu/default.jpg')}}" alt="Immagine GPU" width="200px">
                            <div class="card-body">
                              <a href="{{route('gpu.show', ['gpu' => $gpu, 'model' => $gpu->model])}}" class="card-link">Scopri di più</a>
                            </div>
                            <div class="card-footer text-muted">
                              Pubblicato il: {{date('d/m/Y H:i', strtotime($gpu->updated_at))}}
                            </div>
                          </div>
                    </div>


                    @endforeach

                </div>
            </div>
        </div>
    </div>

</x-layout>