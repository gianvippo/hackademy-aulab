<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cpu extends Model
{
    use HasFactory;

    protected $fillable = [

        'name',
        'brand',
        'frequency',
        'cores',
        'threads',
        'tdp',
        'img',
        'user_id'

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
