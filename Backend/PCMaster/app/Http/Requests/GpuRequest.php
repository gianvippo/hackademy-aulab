<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GpuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'brand' => 'required',
            'model' => 'required',
            'vendor' => 'required|max:8',
        ];
    }

    public function messages(){
        return [

        'brand.required' => "Il campo Brand è necessario.",
        'model.required' => "Il campo Model è necessario.",
        'vendor.required' => "Il campo Produttore è necessario.",
        'brand.unique' => "La GPU è già presente nel DB.",
        'vendor.max' => "Nel campo Produttore puoi inserire massimo 8 caratteri.",

        ];
    }
}
