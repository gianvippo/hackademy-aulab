<?php

namespace App\Http\Controllers;

use App\Http\Requests\GpuRequest;
use App\Models\Gpu;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GpuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }


    public function index()
    {
        $gpus = Gpu::all();
        return view('gpu.index', compact('gpus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gpu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GpuRequest $request)
    {
        try {

            $gpu = Gpu::create([
                'brand' => $request->brand,
                'model' => $request->model,
                'vendor' => $request->vendor,
                'user_id' => Auth::user()->id
            ]);

            if ($request->formFile) $gpu->update([
                'img' => $request->file('formFile')->store('/public/gpu')
            ]);
            if ($request->description) $gpu->update([
                'description' => $request->description
            ]);

            return redirect(route('homepage'))->with('success', 'GPU aggiunta correttamente!');
        } catch (Exception $error) {
            return redirect(route('homepage'))->with('errorMes', `Qualcosa è andato storto. Riprova.\nErrore $error`);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gpu  $gpu
     * @return \Illuminate\Http\Response
     */
    public function show(Gpu $gpu, $model = NULL)
    {
        return view('gpu.show', compact('gpu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gpu  $gpu
     * @return \Illuminate\Http\Response
     */
    public function edit(Gpu $gpu, $model = NULL)
    {
        if ($gpu->user_id != Auth::user()->id) return redirect(route('homepage'))->with('errorMes', `Non puoi accedere a questa pagina.`);

        return view('gpu.edit', compact('gpu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gpu  $gpu
     * @return \Illuminate\Http\Response
     */
    public function update(GpuRequest $request, Gpu $gpu)
    {
        try {

            $gpu->update([
                'brand' => $request->brand,
                'model' => $request->model,
                'vendor' => $request->vendor,
                'user_id' => Auth::user()->id
            ]);

            if ($request->formFile) $gpu->update([
                'img' => $request->file('formFile')->store('/public/gpu')
            ]);
            if ($request->description) $gpu->update([
                'description' => $request->description
            ]);

            return redirect(route('homepage'))->with('success', 'GPU modificata correttamente!');
        } catch (Exception $error) {
            return redirect(route('homepage'))->with('errorMes', `Qualcosa è andato storto. Riprova.\nErrore $error`);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gpu  $gpu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gpu $gpu)
    {
        if ($gpu->user_id != Auth::user()->id) return redirect(route('homepage'))->with('errorMes', `Non puoi accedere a questa pagina.`);

        $gpu->delete();
        return redirect(route('homepage'))->with('success', 'GPU eliminata correttamente!');
    }
}
