<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Cpu;
use Illuminate\Http\Request;
use App\Http\Requests\CpuRequest;
use Illuminate\Support\Facades\Auth;

class CpuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth')->except('index', 'show');
     }
 
 
     public function index()
     {
         $cpus = Cpu::all();
         return view('cpu.index', compact('cpus'));
     }
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cpu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CpuRequest $request)
    {
        try {

            $cpu = Cpu::create([
                'brand' => $request->brand,
                'name' => $request->name,
                'frequency' => $request->frequency,
                'cores' => $request->cores,
                'threads' => $request->threads,
                'tdp' => $request->tdp,
                'user_id' => Auth::user()->id
            ]);

            if ($request->formFile) $cpu->update([
                'img' => $request->file('formFile')->store('/public/cpu')
            ]);

            return redirect(route('homepage'))->with('success', 'CPU aggiunta correttamente!');

        } catch (Exception $error) {
            return redirect(route('homepage'))->with('errorMes', `Qualcosa è andato storto. Riprova.\nErrore $error`);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cpu  $cpu
     * @return \Illuminate\Http\Response
     */
    public function show(Cpu $cpu, $name = NULL)
    {
        return view('cpu.show', compact('cpu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cpu  $cpu
     * @return \Illuminate\Http\Response
     */
    public function edit(Cpu $cpu, $name = NULL)
    {
        if ($cpu->user_id != Auth::user()->id) return redirect(route('homepage'))->with('errorMes', `Non puoi accedere a questa pagina.`);

        return view('cpu.edit', compact('cpu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cpu  $cpu
     * @return \Illuminate\Http\Response
     */
    public function update(CpuRequest $request, Cpu $cpu)
    {
        try {

            $cpu->update([
                'brand' => $request->brand,
                'name' => $request->name,
                'frequency' => $request->frequency,
                'cores' => $request->cores,
                'threads' => $request->threads,
                'tdp' => $request->tdp
            ]);

            if ($request->formFile) $cpu->update([
                'img' => $request->file('formFile')->store('/public/cpu')
            ]);

            return redirect(route('homepage'))->with('success', 'CPU modificata correttamente!');
        } catch (Exception $error) {
            return redirect(route('homepage'))->with('errorMes', `Qualcosa è andato storto. Riprova.\nErrore $error`);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cpu  $cpu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cpu $cpu)
    {
        if ($cpu->user_id != Auth::user()->id) return redirect(route('homepage'))->with('errorMes', `Non puoi accedere a questa pagina.`);

        $cpu->delete();
        return redirect(route('homepage'))->with('success', 'CPU eliminata correttamente!');
    }
}
