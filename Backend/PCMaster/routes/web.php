<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CpuController;
use App\Http\Controllers\GpuController;
use Laravel\Sail\Console\PublishCommand;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'viewHome'])->name('homepage');

Route::get('/gpu', [GpuController::class, 'index'])->name('gpu.index');
Route::get('/gpu/aggiungi-gpu', [GpuController::class, 'create'])->name('gpu.create');
Route::post('/gpu/aggiungi-gpu/store', [GpuController::class, 'store'])->name('gpu.store');
Route::get('/gpu/{gpu}/{model?}', [GpuController::class, 'show'])->name('gpu.show');
Route::get('/gpu/{gpu}/{model?}/edit', [GpuController::class, 'edit'])->name('gpu.edit');
Route::put('/gpu/{gpu}/{model?}/edit/submit', [GpuController::class, 'update'])->name('gpu.update');
Route::delete('/gpu/{gpu}/{model?}/delete', [GpuController::class, 'destroy'])->name('gpu.delete');

Route::get('/cpu', [CpuController::class, 'index'])->name('cpu.index');
Route::get('/cpu/aggiungi-cpu', [CpuController::class, 'create'])->name('cpu.create');
Route::post('/cpu/aggiungi-cpu/store', [CpuController::class, 'store'])->name('cpu.store');
Route::get('/cpu/{cpu}/{name?}', [CpuController::class, 'show'])->name('cpu.show');
Route::get('/cpu/{cpu}/{name?}/edit', [CpuController::class, 'edit'])->name('cpu.edit');
Route::put('/cpu/{cpu}/{name?}/edit/submit', [CpuController::class, 'update'])->name('cpu.update');
Route::delete('/cpu/{cpu}/{name?}/delete', [CpuController::class, 'destroy'])->name('cpu.delete');

