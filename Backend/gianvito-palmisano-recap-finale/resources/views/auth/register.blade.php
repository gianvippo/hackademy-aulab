<x-layout title="Registrati | Recap">


    <x-header>
        <h1 class="display-3 text-white">Registrati
        </h1>
    </x-header>

    <div class="container-my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <form method="POST" action="{{route('register')}}">
                    @csrf
                    <x-errors />

                    <div class="form-group my-3">
                      <label for="email">Email</label>
                      <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Inserisci email">
                    </div>

                    <div class="form-group mb-3">
                        <label for="name">Nome</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Inserisci il nome">
                      </div>

                    <div class="form-group mb-3">
                      <label for="password">Password</label>
                      <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>

                    <div class="form-group mb-5">
                        <label for="password_confirmation">Conferma Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Conferma Password">
                      </div>

                    <button type="submit" class="btn btn-primary px-4 d-flex mx-auto">Registrati</button>
                  </form>
            </div>
        </div>
    </div>
    
</x-layout>