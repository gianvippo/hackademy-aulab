<x-layout title="Login | Recap">


    <x-header>
        <h1 class="display-3 text-white">Accedi</h1>
    </x-header>
    
    <div class="container-my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <form method="POST" action="{{route('login')}}">
                    @csrf
                    <x-errors />
                    <div class="form-group my-3">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Inserisci email">
                      </div>

                      <div class="form-group mb-3">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                      </div>
  
                      <button type="submit" class="btn btn-primary px-4 d-flex mx-auto">Login</button>
                    </form>
            </div>
        </div>
    </div>

</x-layout>