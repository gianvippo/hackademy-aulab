<x-layout title="Inserisci Libreria | Recap">

    <x-header>
        <h1 class="display-3 text-white">Inserisci una libreria</h1>
    </x-header>

    <div class="container-my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">

                @livewire('create-library-form')

            </div>
        </div>
    </div>

</x-layout>