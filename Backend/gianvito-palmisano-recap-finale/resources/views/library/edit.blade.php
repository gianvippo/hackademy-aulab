<x-layout title="Modifica Libreria | Recap">

    <x-header>
        <h1 class="display-3 text-white">Modifica la libreria {{$library->name}}</h1>
    </x-header>

    <div class="container-my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">

                @livewire('edit-library-form', ['libraryId' => $library->id])

            </div>
        </div>
    </div>

</x-layout>