<x-layout title="Index Librerie | Recap">

    <x-header>
        <h1 class="display-3 text-white">Librerie</h1>
    </x-header>

    <x-session-messages />

    @livewire('index-libraries')

</x-layout>