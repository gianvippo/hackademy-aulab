<x-layout title="{{$library->name}} | Recap">

    <x-header>
        <h1 class="display-3 text-white">{{$library->name}}</h1>
    </x-header>

    <div class="container min-vh-100 mt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <img src="{{$library->image ? Storage::url($library->image) : Storage::url('/library/default.jpg')}}" class="img-fluid">
                <p class="small fst-italic mt-3">Indirizzo: {{$library->address}}</p>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-12 col-md-5">
                <p>Descrizione: {{$library->description}}</p>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-12 col-md-5">
                <p>Aggiunto da: {{$library->user->name}}</p>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-12 col-md-5 text-center">
                <a href="{{route('library.index')}}" class="btn btn-primary">Torna indietro</a>
                @if (Auth::user() && Auth::user()->id == $library->user->id)
                <a href="{{route('library.edit', compact('library'))}}" class="btn btn-secondary">Modifica</a>
                @endif
            </div>
        </div>
    </div>
</x-layout>