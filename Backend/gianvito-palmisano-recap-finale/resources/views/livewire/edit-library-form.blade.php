<div class="my-3">
    <form wire:submit.prevent="update" enctype="multipart/form-data">

        @csrf

        <x-errors />
        <x-session-messages />

        <div class="form-group my-3">
            <label class="form-label" for="name">Nome della libreria</label>
            <input type="text" wire:model="name" class="form-control" id="name"
                placeholder="Inserisci il nome della libreria">
        </div>

        <div class="form-group my-3">
            <label class="form-label" for="address">Indirizzo</label>
            <input type="text" wire:model="address" class="form-control" id="address"
                placeholder="Inserisci l'indirizzo">
        </div>

        <div class="my-3">
            Immagine esistente:
            <img width="200" src="{{ $old_image ?? Storage::url('/library/default.jpg') }}">
        </div>

        <div class="form-group my-3">
            <label class="form-label" for="image">Foto</label>
            <input type="file" wire:model="image" class="form-control" id="image" placeholder="Inserisci l'indirizzo">
        </div>

        @if($image)
        <div class="my-3">
            Anteprima
            <img width="200" src="{{ $image->temporaryUrl() }}">
        </div>
        @endif

        <div class="form-group my-3">
            <label class="form-label" for="description" class="form-label">Descrizione</label>
            <textarea wire:model="description" id="description" class="form-control" cols="30" rows="4"></textarea>
        </div>

        <div class="d-flex justify-content-between">
            <a href="{{route('library.index')}}" class="btn btn-primary">Torna all'indice delle librerie</a>
            <button type="submit" onclick="window.scrollTo({ top: 0 });" class="btn btn-primary">Modifica la libreria <i class="fa-solid fa-plus"></i></button>
            <button type="button" wire:click="delete" class="btn btn-danger">Elimina la libreria <i class="fa-solid fa-xmark"></i></button>
        </div>
    </form>
</div>