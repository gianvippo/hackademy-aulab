<div class="container my-5">
    <div class="row justify-content-center">

        @forelse ($libraries as $library)
            <div class="col-12 col-md-4">
                <div class="card">
                    <img class="card-img-top" src="{{$library->image ? Storage::url($library->image) : Storage::url('/library/default.jpg')}}" alt="Immagine Libreria">
                    <div class="card-body">
                      <h5 class="card-title">{{$library->name}}e</h5>
                      <p class="card-text">{{$library->address}}</p>
                    </div>
                    <a href="{{route('library.show', compact('library'))}}" class="btn btn-primary px-4 d-flex mx-auto">Scopri di più</a>
                  </div>
            </div>
        @empty
            <div class="col-12">
                <h2>Non sono presenti librerie.</h2>
            </div>
        @endforelse
    </div>
</div>