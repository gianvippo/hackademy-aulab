<div class="my-3">
    <form wire:submit.prevent="store" enctype="multipart/form-data">

        @csrf

        <x-errors />
        <x-session-messages />

        <div class="form-group my-3">
            <label class="form-label" for="name">Nome della libreria</label>
            <input type="text" wire:model="name" class="form-control" id="name"
                placeholder="Inserisci il nome della libreria">
        </div>

        <div class="form-group my-3">
            <label class="form-label" for="address">Indirizzo</label>
            <input type="text" wire:model="address" class="form-control" id="address"
                placeholder="Inserisci l'indirizzo">
        </div>

        <div class="form-group my-3">
            <label class="form-label" for="image">Foto</label>
            <input type="file" wire:model="image" class="form-control" id="image" placeholder="Inserisci l'indirizzo">
        </div>

        @if($image)
        <div class="my-3">
            Anteprima
            <img width="200" src="{{ $image->temporaryUrl() }}">
        </div>
        @endif

        <div class="form-group my-3">
            <label class="form-label" for="description" class="form-label">Descrizione</label>
            <textarea wire:model="description" id="description" class="form-control" cols="30" rows="4"></textarea>
        </div>

        <button type="submit" class="btn btn-primary px-4 d-flex mx-auto">Inserisci la libreria</button>
    </form>
</div>