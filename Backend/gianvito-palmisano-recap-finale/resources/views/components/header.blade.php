<div class="container-fluid p-5 bg-primary text-center">
    <div class="row justify-content-center">
        <div class="col-12">
            {{$slot}}
        </div>
    </div>
</div>