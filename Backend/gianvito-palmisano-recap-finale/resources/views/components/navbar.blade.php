<nav class="navbar navbar-expand-lg bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{route('homepage')}}">Recap</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{route('homepage')}}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('library.index')}}">Librerie</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('library.create')}}">Aggiungi una libreria</a>
        </li>
      </ul>
        @guest
        <ul class="navbar-nav">
          <li class="nav-item" title="Login">
            <a class="nav-link d-flex" href="{{route('login')}}">
              <div>Login</div>
              <i class="fa-solid fa-right-to-bracket fs-4 mx-2"></i>
            </a>
          </li>
          <li class="nav-item" title="Registrati">
            <a class="nav-link d-flex" href="{{route('register')}}">
              <div>Registrati</div>
              <i class="fa-solid fa-user-plus fs-4 mx-2"></i>
            </a>
          </li>
        </ul>
        @else
        <ul class="navbar-nav">
          <li class="nav-item my-auto">
            <p class="my-auto">Bentornato {{Auth::user()->name}}</p>
          </li>
          <li class="nav-item" title="Logout">
            <a class="nav-link d-flex" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();"
              href="{{route('logout')}}">
              <form id='form-logout' method='POST' action='{{route('logout')}}' class='d-none'>@csrf</form>
              <div>Logout</div>
              <i class="fa-solid fa-right-from-bracket fs-4 mx-2"></i>
            </a>
          </li>
        </ul>
        @endguest
      </ul>
    </div>
  </div>
</nav>