<?php

namespace App\Http\Livewire;

use App\Models\Library;
use Livewire\Component;

class IndexLibraries extends Component
{
    public function render()
    {
        $libraries = Library::all();
        return view('livewire.index-libraries', compact('libraries'));
    }
}
