<?php

namespace App\Http\Livewire;

use App\Models\Library;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditLibraryForm extends Component
{
    use WithFileUploads;
    
    public $libraryId, $name, $address, $old_image, $description, $image;
    
    public function mount()
    {
        $library = Library::find($this->libraryId);
    
        $this->name = $library->name;
        $this->address = $library->address;
        $this->old_image = Storage::url($library->image);
        $this->description = $library->description;
    }
    
    public function update()
    {
        $library = Library::find($this->libraryId);

        $library->update([
            'name' => $this->name,
            'address' => $this->address,
            'description' => $this->description,
        ]);

        if ($this->image) {
            $library->update([
                'image' => $this->image->store('/public/library'),
            ]);

            $this->old_image = $this->image->temporaryUrl();

            $this->reset('image');

        }

        session()->flash('successMessage', 'Hai correttamente modificato la libreria.');
    }

    public function delete()
    {
        Library::find($this->libraryId)->delete();

        return redirect(route('library.index'))->with('successMessage', 'Hai correttamente cancellato la libreria');

    }

    public function render()
    {
        return view('livewire.edit-library-form');
    }
}
