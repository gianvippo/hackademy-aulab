<?php

namespace App\Http\Livewire;

use App\Models\Library;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class CreateLibraryForm extends Component
{
    use WithFileUploads;

    public $name, $address, $image, $description;

    public function store()
    {
        $library = Library::create([
            'name' => $this->name,
            'address' => $this->address,
            'description' => $this->description,
            'user_id' => Auth::user()->id
        ]);

        if ($this->image) $library->update([
            'image' => $this->image->store('/public/library'),
        ]);


        session()->flash('successMessage', 'Hai correttamente inserito la libreria.');
        $this->reset();
    }

    public function render()
    {
        return view('livewire.create-library-form');
    }
}
