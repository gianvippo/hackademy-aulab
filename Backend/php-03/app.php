<?php

$carta = [];

class Bevanda
{

    public $id;
    public $nome;
    public $bicchiere;
    private $prezzo;
    public static $cont = 0;

    public function __construct($nome, $bicchiere, $prezzo)
    {
        $this->nome = $nome;
        $this->bicchiere = $bicchiere;
        $this->prezzo = $prezzo;
        self::$cont++;
        $this->id = self::$cont;

    }
    
    public static function displayCont()
    {
        echo "Numero bevande create ".self::$cont."\n";
    }

    public function IVA() {
        $this->prezzo = ($this->prezzo * 1.22);
    }
}

class Alcolici extends Bevanda
{
    public $gradi;

    public function __construct($nome, $bicchiere, $prezzo, $gradi)
    {
        parent::__construct($nome, $bicchiere, $prezzo);
        $this->gradi = $gradi;
    }
}

class Cocktail extends Alcolici
{
    public $ghiaccio;
    public $cannuccia;
    public $ingredienti;

    public function __construct($nome, $bicchiere, $prezzo, $gradi, $ghiaccio, $cannuccia, $ingredienti)
    {
        parent::__construct($nome, $bicchiere, $prezzo, $gradi);
        $this->ghiaccio = $ghiaccio;
        $this->cannuccia = $cannuccia;
        $this->ingredienti = $ingredienti;
    }
}

class Birra extends Alcolici
{
    public $tipo;
    public $produzione;
    public $origine;
    
    public function __construct($nome, $bicchiere, $prezzo, $gradi, $tipo, $produzione, $origine)
    {
        parent::__construct($nome, $bicchiere, $prezzo, $gradi);
        $this->tipo = $tipo;
        $this->produzione = $produzione;
        $this->origine = $origine;
    }
}

class Amaro extends Alcolici
{
    public $produzione;
    public $origine;
    public $temperatura;
    
    public function __construct($nome, $bicchiere, $prezzo, $gradi, $produzione, $origine, $temperatura)
    {
        parent::__construct($nome, $bicchiere, $prezzo, $gradi);
        $this->produzione = $produzione;
        $this->origine = $origine;
        $this->temperatura = $temperatura;
    }
}

array_push($carta, new Cocktail("americano","DOF", 5, 20, true, true, ["campari","vermouth","soda","arancia"]),
new Birra("ichnusa","bottiglia", 6, 4.7, "chiara", "industriale","Sardegna"),
new Bevanda("acqua","DOF",0.25),
new Cocktail("miami","long_drink", 6, 20, true, true, ["vodka","rum","gin","blue_curacao","zucchero","tonica"]),
new Amaro("lucano","old_fashioned", 3, 35, "industriale","Basilicata","freddo"));

print_r($carta);

Bevanda::displayCont();

$carta[4]->IVA();

print_r($carta[4]);



