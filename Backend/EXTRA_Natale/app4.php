<?php

$stati = [];
$check = true;

class State
{
    public $id;
    public $name;
    public $min_age;
    public static $cont = 0;

    public function __construct($nome, $eta)
    {
        $this->name = $nome;
        $this->min_age = $eta;
        self::$cont++;
        $this->id = self::$cont;
    }

    public function calc_license($eta)
    {
        if ($eta >= $this->min_age) echo "\nPuoi guidare in " . $this->name;
        else echo "\nNON puoi guidare in " . $this->name;
    }
}

array_push($stati, new State("Italia", 18), new State("Francia", 19), new State("USA", 21), new State("Cambogia", 12));

while (true) {

    $age = readline("Inserisci la tua età: ");

    if (is_numeric($age) && $age > 0) {

        while ($check) {

            $input = strtolower(readline("Inserisci lo stato di residenza: "));

            foreach ($stati as $stato) {

                $state = strtolower($stato->name);

                if ($input == $state) {
                    $stato->calc_license($age);
                    $check = false;
                    break;
                }
            }

            if($check) echo "!!! - Lo stato che hai inserito non è presente nel DB, riscrivi - !!!\n\n";

        }
        break;
    } else echo "!!! - Non hai scritto un numero, riscrivi - !!!\n\n";
}
