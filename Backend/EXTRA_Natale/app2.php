<?php

while (true) {

    $number = readline("Inserisci il voto: ");
    if (is_numeric($number) && $number > 0 && $number < 11) {

        switch (true) {

            case $number > 8:
                echo "\nVoto Ottimo!\n";
                break;
            case $number > 7:
                echo "\nVoto Distinto!\n";
                break;
            case $number >= 6:
                echo "\nVoto Sufficiente!\n";
                break;
            case $number < 6:
                echo "\nVoto Insufficiente!\n";
                break;
        }

        break;
    } else echo "!!! - Non hai scritto un numero valido, riscrivi - !!!\n\n";
}
