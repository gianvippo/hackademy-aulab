<?php

$array = [];
$check = true;

function sufficienza($studente)
{
    if($studente["grade"]>=60) return $studente;
}


while($check) {

    $numbers = readline("Inserisci il numero di studenti: ");

    if(is_numeric($numbers)) {

    for($i=0;$i<$numbers;$i++) {

        $student = [];
    
        while(true) {

            $input = readline("\nInserisci il nome dello studente alla posizione $i: ");
            if(ctype_alpha($input)) {
                
                $student["name"] = $input;
                break;
    
            }
            else echo "!!! - Non hai scritto un nome valido, riscrivi - !!!\n\n";
        }

        while(true) {

            $input = readline("\nInserisci il voto dello studente alla posizione $i: ");
            if(is_numeric($input) && $input>0 && $input < 110) {
                
                $student["grade"] = (int)$input;
                break;
    
            }
            else echo "!!! - Non hai scritto un voto valido, riscrivi - !!!\n\n";
        }

        $array[$i] = $student;
    
    }

    $check = false;

    } 
    else echo "!!! - Non hai scritto un numero, riscrivi - !!!\n\n";
    
}

$sufficienti = array_filter($array, "sufficienza");

print_r($sufficienti);

