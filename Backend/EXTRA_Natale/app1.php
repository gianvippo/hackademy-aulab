<?php

$numeri = [];

for ($i=0; $i < 3; $i++) { 

    while(true) {

        $number = readline("Inserisci il numero in posizione ".++$i.": ");
        $i--;    
        if(is_numeric($number)) {
            array_push($numeri, $number);
            break; 
        }
        else echo "!!! - Non hai scritto un numero, riscrivi - !!!\n\n";

    }
}

$max = max($numeri);

$index = array_search($max, $numeri);

switch($index) {

    case 0: echo "\nIl maggiore è il primo.\n"; break;
    case 1: echo "\nIl maggiore è il secondo.\n"; break;
    case 2: echo "\nIl maggiore è il terzo.\n"; break;

}