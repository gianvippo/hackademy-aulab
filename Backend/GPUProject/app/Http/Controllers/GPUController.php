<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Gpu;
use App\Mail\MailController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\FormGPURequest;

class GPUController extends Controller
{
    public function viewHome()
    {
        $gpus = Gpu::all();
        return view('index', ['gpus' => $gpus]);
    }

    public function viewAddGPU()
    {
        return view('gpu/add-gpu');
    }

    public function viewIndexGPU()
    {
        $gpus = Gpu::all();
        return view('gpu/all-gpu', ['gpus' => $gpus]);
    }

    public function submitGPU(FormGPURequest $request)
    {
        $brand = $request->brand;
        $model = $request->model;

        $data = compact('brand', 'model');

        if (isset($request->email)) {

            try {
                if (!isset($request->formFile)) Gpu::create([
                    'brand' => $brand,
                    'model' => $model,
                    'email' => $request->email
                ]);
                else {

                    $file = $request->file('formFile')->store('/public/img');

                    Gpu::create([
                        'brand' => $brand,
                        'model' => $model,
                        'email' => $request->email,
                        'img' => $file
                    ]);
                }

                $data['file'] = $file;

                Mail::to($request->email)->send(new MailController($data));
                return redirect(route('homepage'))->with('success', 'GPU aggiunta e mail inviata correttamente!');
            } catch (Exception $error) {
                return redirect(route('homepage'))->with('danger', `Qualcosa è andato storto, riprova!\nErrore $error`);
            }
        } else {

            try {
                if (!isset($request->formFile)) Gpu::create([
                    'brand' => $brand,
                    'model' => $model,
                ]);
                else Gpu::create([
                    'brand' => $brand,
                    'model' => $model,
                    'img' => $request->file('formFile')->store('/public/img')
                ]);

                return redirect(route('homepage'))->with('success', 'GPU aggiunta correttamente!');
            } catch (Exception $error) {
                return redirect(route('homepage'))->with('danger', `Qualcosa è andato storto, riprova!\nErrore $error`);
            }
        }
    }
}
