<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormGPURequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'brand' => 'required',
            'model' => 'required',
            'formFile' => 'image|max:1024',
        ];
    }

    public function messages(){
        return [
    
        'brand.required' => "Devi inserire la Marca",
        'model.required' => "Devi inserire il Modello",
        'formFile.image' => "Il file deve essere un'immagine",
        'formFile.max' => "Il file non può superare i 1MB"
    
        ];
    }
    
}
