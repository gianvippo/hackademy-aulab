<?php

use App\Http\Controllers\GPUController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GPUController::class, 'viewHome'])->name('homepage');

Route::get('/aggiungi-gpu', [GPUController::class, 'viewAddGPU'])->name('add.gpu');
Route::post('/aggiungi-gpu/submit', [GPUController::class, 'submitGPU'])->name('submit.gpu');

Route::get('/index-gpu', [GPUController::class, 'viewIndexGPU'])->name('index.gpu');
