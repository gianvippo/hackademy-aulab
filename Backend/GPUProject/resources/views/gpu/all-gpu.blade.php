<x-layout title="Tutte le GPU | GPUProject">

    <div class="container min-vh-100">
        <div class="row mt-5">
            <div class="col text-center">
                <h1 class="display-1">Tutte le GPU</h1>
            </div>
        </div>
        <div class="row mt-5 justify-content-around">

            @foreach ($gpus as $gpu)
            <div class="col-12 col-md-2">
                <x-card numbId="{{$gpu->id}}" brand="{{$gpu->brand}}" model="{{$gpu->model}}"
                    email="{{$gpu->email}}" img="{{$gpu->img}}" date="{{$gpu->updated_at}}" />
            </div>
            @endforeach



        </div>



</x-layout>