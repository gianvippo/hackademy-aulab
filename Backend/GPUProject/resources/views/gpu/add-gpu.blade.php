<x-layout title="Aggiungi GPU | GPUProject">

    <div class="container min-vh-100">
        <div class="row mt-5">
            <div class="col text-center">
                <h1 class="display-1">Aggiungi GPU</h1>
            </div>
        </div>
        <div class="row mt-5 justify-content-around">
            <div class="col-12 col-md-6">
                <x-form />
            </div>
        </div>
    </div>



</x-layout>