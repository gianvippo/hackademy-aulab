<form action="{{route('submit.gpu')}}" method="POST" enctype="multipart/form-data">
    @csrf
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <fieldset>
        <div class="brand-group">
            <label for="brand" class="form-label mt-4">Brand</label>
            <select name="brand" class="form-select" id="brand">
                <option>Seleziona...</option>
                <option value="nvidia">NVIDIA</option>
                <option value="amd">AMD</option>
            </select>
        </div>
        <div id="model-form">
            <label for="model" class="form-label mt-4">GPU</label>
            <select name="model" class="form-select" id="model" disabled="">
            </select>
        </div>
        <div class="form-group">
            <label for="formFile" class="form-label mt-4">Inserisci l'immagine</label>
            <input name="formFile" class="form-control" type="file" id="formFile">
        </div>
        <div class="form-group">
            <label for="email" class="form-label mt-4">Email address</label>
            <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp"
                placeholder="Inserisci la tua email">
            <small id="emailHelp" class="form-text text-muted">Non condivideremo con nessumo la tua mail.</small>
        </div>
        <button type="submit" class="btn btn-primary mx-auto d-block mt-3">Invia</button>
    </fieldset>
</form>

<script>
    let nvidiaGPU = ["3060", "3060 Ti", "3070 Ti", "3080", "3090"]
    let amdGPU = ["6600", "6600 XT", "6700", "6700 XT", "6800"]
    let brand = document.querySelector('#brand')

    function createList(list) {
        let model = document.querySelector('#model')
        model.removeAttribute("disabled")
        model.innerHTML = "";
        list.forEach(element => {
            let option = document.createElement('option')
            option.value = element.split(' ').join('-').toLowerCase()
            option.innerHTML = element
            model.appendChild(option)
        });
    }

    brand.addEventListener("click", () => {

        switch (brand.value) {
            case 'nvidia':
                createList(nvidiaGPU)
                break;

            case 'amd':
                createList(amdGPU)
                break;

            default:
                let model = document.querySelector('#model')
                model.setAttribute("disabled", "")
                model.innerHTML = ""
                let option = document.createElement('option')
                model.appendChild(option)
                break;
        }

    })
</script>