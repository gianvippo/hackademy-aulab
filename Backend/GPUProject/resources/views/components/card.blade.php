<div class="card mb-3">
    <h3 class="card-header">{{$model}}</h3>
    <img src=" @if ($img)
    {{Storage::url($img)}} @else https://picsum.photos/300 @endif" alt="">
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Brand: {{$brand}}</li>
      <li class="list-group-item">Inserito da: @if ($email)
        {{$email}} @else Anonimo @endif</li>
    </ul>
    <div class="card-footer text-muted">
      Data: {{$date}}
    </div>
  </div>