<x-layout title="Homepage | GPUProject">

    <div class="container min-vh-100">
        <div class="row mt-5">
            <div class="col text-center">
                <h1 class="display-1">Homepage</h1>
                <h2 class="fs-1 fst-italic">Benvenuti nel sito di GPU</h2>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col text-center">
                <h3>Ecco le ultime GPU inserite</h2>
            </div>
        </div>
        @if (session('success'))
        <div class="row">
            <div class="col">
                <h3 class="alert alert-success">{{session('success')}}</h3>
            </div>
        </div>
        @endif
        @if (session('danger'))
        <div class="row">
            <div class="col">
                <h3 class="alert alert-danger">{{session('danger')}}</h3>
            </div>
        </div>
        @endif
        <div class="row mt-5 justify-content-around">
            @for ($i = 0; $i < 5; $i++) @if (isset($gpus[$i])) <div class="col-12 col-md-2">
                <x-card numbId="{{$gpus[$i]->id}}" brand="{{$gpus[$i]->brand}}" model="{{$gpus[$i]->model}}"
                    email="{{$gpus[$i]->email}}" img="{{$gpus[$i]->img}}" date="{{$gpus[$i]->updated_at}}" />
        </div>
        @endif
        @endfor


    </div>



</x-layout>