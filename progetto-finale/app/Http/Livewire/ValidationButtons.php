<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ValidationButtons extends Component
{
    public $article;

    public function render()
    {
        return view('livewire.validation-buttons', ['article' => $this->article]);
    }

    public function revisorChoice($bool)
    {
        $this->dispatchBrowserEvent('choiceAd', ['id' => $this->article->id, 'bool' => $bool, 'lang' => app()->getLocale()]);
        $this->article->setAccepted($bool, $this->article);
    }

}
