<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RevisorCard extends Component
{
    public $article;

    public function check($image)
    {
        $danger = 0;
        $warning = 0;
        $success = 0;
        $class = '';

        foreach ($image->getAttributes() as $item) {
            if ($item == 'text-danger fas fa-circle') $danger++;
            elseif ($item == 'text-warning fas fa-circle') $warning++;
            else $success++;
        }

        if ($danger) $class = 'fs-3 text-danger fas fa-circle';
        elseif ($warning) $class = 'fs-3 text-warning fas fa-circle';
        else $class = 'fs-3 text-success fas fa-circle';

        return $class;
    }

    public function render()
    {
        return view('livewire.revisor-card');
    }
}
