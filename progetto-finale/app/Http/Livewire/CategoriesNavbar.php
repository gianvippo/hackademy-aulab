<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;

class CategoriesNavbar extends Component
{
    public function render()
    {
        switch (app()->getLocale()){
            case 'it':
                $column = 'nameIt';
                break;

            case 'es':
            $column = 'nameEs';
                break;
                    
            default:
                $column = 'nameEn';
                break;
        }
        
        $categories = Category::all()->sortBy($column);
        return view('livewire.categories-navbar', compact('categories'));
    }
}
