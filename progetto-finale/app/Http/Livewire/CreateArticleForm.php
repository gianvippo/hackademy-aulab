<?php

namespace App\Http\Livewire;

use App\Jobs\ApplyWatermark;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use App\Jobs\RemoveFaces;
use App\Models\Article;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\ResizeImage;


use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class CreateArticleForm extends Component
{
    use WithFileUploads;

    public $title, $price, $category, $description;
    public $temporary_images;
    public $images = [];

    protected $rules = [

        'title' => 'required|max:30|min:6',
        'price' => 'required|numeric|max_digits:8|min:1',
        'description' => 'required|max:500|min:15',
        'images.*' => 'image|max:1024',
        'temporary_images.*' => 'image|max:1024',
        'temporary_images' => 'max:4',

    ];

    protected $messages = [

        'title.required' => 'Manca il titolo',

        'price.required' => 'Non hai inserito il prezzo',

        'description.required' => 'Manca la descrizione',
        'temporary_images.max' => 'Puoi inserire massimo 4 immagini', 

    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function store()
    {

        $lang = app()->getLocale();

        $this->validate();

        $article = Article::create([
            'title' => $this->title,
            'price' => $this->price,
            'description' => $this->description,
            'user_id' => Auth::user()->id,
        ]);

        if ($this->category) {
            $article->update([
                'category_id' => $this->category
            ]);
        }

        if (count($this->images)) {
            foreach ($this->images as $image) {
                $newFileName = "articles/{$article->id}";
                $newImage = $article->images()->create(['path' => $image->store($newFileName, 'public')]);

                RemoveFaces::withChain([
                    new ApplyWatermark($newImage->id),
                    new ResizeImage($newImage->path, 400, 400),
                    new GoogleVisionSafeSearch($newImage->id),
                    new GoogleVisionLabelImage($newImage->id),
                ])->dispatch($newImage->id);
            }

            File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }

        switch ($lang) {
            case 'it':
                session()->flash('successMsg', 'Hai correttamente inserito l\'annuncio. Sarai informato via mail sull\'esito della revisione da parte di un membro del nostro team');
                break;

            case 'es':
                session()->flash('successMsg', 'Ha colocado correctamente su anuncio. Un miembro de nuestro equipo le notificará por correo electrónico el resultado de la revisión');
                break;

            default:
                session()->flash('successMsg', 'You have successfully placed your ad. You will be notified via email of the outcome of the review by a member of our team');
                break;
        }
        
        $this->reset();
    }

    public function updatedTemporaryImages()
    {
        if ($this->validate([
            'temporary_images.*' => 'image|max:1024',
        ])) {
            foreach ($this->temporary_images as $image) {
                $this->images[] = $image;
            }
        }
    }

    public function removeImage($key)
    {
        if (in_array($key, array_keys($this->images))) {
            unset($this->images[$key]);
        }
    }

    public function render()
    {
        $categories = Category::all();
        return view('livewire.create-article-form', compact('categories'));
    }
}
