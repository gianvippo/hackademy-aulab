<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Mail\ContactArticle;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ContactModal extends Component
{
    public $article, $message_text, $checkbox;

    protected $rules = [
        'message_text' => 'required|max:100|min:6',
        'checkbox' => 'required|accepted',
    ];

    public function mount()
    {
        $lang = app()->getLocale();

        switch ($lang) {
            case 'it':
                $this->message_text = 'Ciao ' . $this->article->user->name . ', ti contatto per l\'annuncio ' . $this->article->title . ', è ancora disponibile?';
                break;

            case 'es':
                $this->message_text = 'Hola ' . $this->article->user->name . ', te contacto por el anuncio ' . $this->article->title . ', ¿sigue disponible?';
                break;

            default:
                $this->message_text = 'Hi ' . $this->article->user->name . ', I\'m contacting you about the ' . $this->article->title . ' ad, is it still available?';
                break;
        }
    }

    public function render()
    {
        return view('livewire.contact-modal');
    }

    public function send()
    {
        $this->validate();
        Mail::to($this->article->user->email)->send(new ContactArticle(Auth::user(), $this->article, $this->message_text));
        $this->dispatchBrowserEvent('msgSend', ['lang' => app()->getLocale()]);
    }
}
