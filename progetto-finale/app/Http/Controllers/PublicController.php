<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function homepage()
    {
        return view('homepage');
    }

    public function workWithUs()
    {
        return view('work-with-us');
    }

    public function searchArticle(Request $request)
    {
        
        $categories = Category::all();
        $articles = Article::search($request->searched)->where('is_accepted', true)->get();

        return view('article.index', ['articles' => $articles, 'categories' => $categories]);
    }

    public function setLanguage($lang)
    {
        session()->put('locale', $lang);
        return redirect()->back();
    }
}
