<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Article;
use App\Mail\BecomeRevisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;

class RevisorController extends Controller
{
    public function index()
    {
        $articles_to_check = Article::where('is_accepted', null)->orderBy('created_at', 'DESC')->paginate(10);
        return view('revisor.index', compact('articles_to_check'));
    }

    public function archive()
    {
        $articles_checked = Article::orderBy('created_at', 'DESC')->get();
        return view('revisor.archive', compact('articles_checked'));
    }

    public function acceptArticle(Article $article)
    {

        $article->setAccepted(true, $article);

        switch (app()->getLocale()) {
            case 'it':
                $successMsg = 'Annuncio accettato';
                break;

            case 'es':
                $successMsg = 'Anuncio aceptado';
                break;

            default:
                $successMsg = 'Accepted announcement';
                break;
        }
        return redirect()->back()->with('successMsg', $successMsg);
    }

    public function rejectArticle(Article $article)
    {

        $article->setAccepted(false, $article);
        switch (app()->getLocale()) {
            case 'it':
                $successMsg = 'Annuncio rifiutato';
                break;

            case 'es':
                $successMsg = 'Anuncio no aceptado';
                break;

            default:
                $successMsg = 'Rejected announcement';
                break;
        }
        return redirect()->back()->with('successMsg', $successMsg);
    }

    public function cancelAction(Article $article)
    {
        $article->setAccepted(null);
        switch (app()->getLocale()) {
            case 'it':
                $successMsg = 'Annuncio ripristinato';
                break;

            case 'es':
                $successMsg = 'Anuncio restablecido';
                break;

            default:
                $successMsg = 'Restored announcement';
                break;
        }
        return redirect()->back()->with('successMsg', $successMsg);
    }

    public function becomeRevisor(Request $request)
    {
        $lang = app()->getLocale();
        $successMsg = '';

        switch ($lang) {
            case 'it':
                $successMsg = 'Hai inviato la richiesta correttamente, aspetta l\'approvazione';
                break;

            case 'es':
                $successMsg = 'Ha enviado la solicitud con éxito, espere la aprobación';
                break;

            default:
                $successMsg = 'You have sent the request successfully, wait for approval';
                break;
        }
        
        $description = $request->description;
        Mail::to('admin@presto.it')->send(new BecomeRevisor(Auth::user(), $description));
        return redirect(route('work_with_us'))->with('successMsg', $successMsg);
    }

    public function makeRevisor(User $user)
    {
        $lang = app()->getLocale();
        $successMsg = '';

        switch ($lang) {
            case 'it':
                $successMsg = 'L\'utente è diventato revisore';
                break;

            case 'es':
                $successMsg = 'El usuario se ha convertido en revisor';
                break;

            default:
                $successMsg = 'The user has become a reviewer';
                break;
        }

        Artisan::call('presto:makeUserRevisor', ["email" => $user->email]);
        return redirect(route('homepage'))->with('successMsg', $successMsg);
    }
}
