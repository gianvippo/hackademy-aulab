<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function profile()
    {
        $articles = Auth::user()->articles_favorite;
        return view('profile', compact('articles'));
    }

    public function myArticle()
    {
        $articles = Auth::user()->articles;
        return view('my-article', compact('articles'));
    }

    public function deleteArticle(Article $article)
    {
        $lang = app()->getLocale();
        $dangerMsg = '';
        $successMsg = '';

        switch ($lang) {
            case 'it':
                $dangerMsg = 'Non puoi accedere a questa pagina';
                $successMsg = 'Articolo eliminato con successo';
                break;

            case 'es':
                $dangerMsg = 'No puedes acceder a esta página';
                $successMsg = 'Artículo eliminado correctamente';
                break;

            default:
                $dangerMsg = 'You cannot access this page';
                $successMsg = 'Article deleted successfully';
                break;
        }

        if (Auth::user()->id != $article->user->id) return redirect()->back()->with('dangerMsg', $dangerMsg);
        else {

            foreach ($article->users as $user) {
                $article->users()->detach($user->id);
            }

            $article->delete();

            return redirect(route('myArticle'))->with('successMsg', $successMsg);
        }
    }

    public function likeArticle(Article $article)
    {
        $lang = app()->getLocale();
        $successMsg = '';

        switch ($lang) {
            case 'it':
                $successMsg = 'Articolo aggiunto ai Preferiti';
                break;

            case 'es':
                $successMsg = 'Artículo añadido a Favoritos';
                break;

            default:
                $successMsg = 'Article added to Favorites';
                break;
        }

        Auth::user()->articles_favorite()->attach($article->id);
        return redirect(route('article.show', compact('article')))->with('successMsg', $successMsg);
    }

    public function unlikeArticle(Article $article)
    {
        $lang = app()->getLocale();
        $successMsg = '';

        switch ($lang) {
            case 'it':
                $successMsg = 'Articolo rimosso dai Preferiti';
                break;

            case 'es':
                $successMsg = 'Artículo eliminado de Favoritos';
                break;

            default:
                $successMsg = 'Article removed from Favorites';
                break;
        }

        Auth::user()->articles_favorite()->detach($article->id);
        return redirect(route('article.show', compact('article')))->with('successMsg', $successMsg);
    }
}
