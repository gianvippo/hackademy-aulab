<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class removeUserRevisor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'presto:removeUserRevisor {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove the revisor role from a user';

    public function __construct()
    {
        parent::__construct();
    }
    
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->first();
        if(!$user) {
            $this->error('User not found.');
            return;
        }

        $user->is_revisor = false;
        $user->save();
        $this->info("The user {$user->name} is no a revisor anymore.");
    }
}
