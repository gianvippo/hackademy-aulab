<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeUserAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'presto:makeUserAdmin {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give to a user the admin role';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('email'))->first();
        if(!$user) {
            $this->error('User not found.');
            return;
        }

        $user->is_admin = true;
        $user->save();
        $this->info("The user {$user->name} is now an admin.");
    }
}
