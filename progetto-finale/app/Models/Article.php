<?php

namespace App\Models;

use App\Models\User;
use App\Models\Image;
use App\Models\Category;
use App\Mail\AcceptedArticle;
use App\Mail\RejectedArticle;
use Laravel\Scout\Searchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory, Searchable;

    protected $fillable =
    [
        'title',
        'price',
        'category_id',
        'description',
        'user_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function toBeRevisionedCount()
    {
        return Article::where('is_accepted', null)->count();
    }

    public function setAccepted($bool, Article $article = NULL)
    {
        $this->is_accepted = $bool;
        $this->save();

        if($this->is_accepted) Mail::to($article->user->email)->send(new AcceptedArticle($article));
        elseif ($this->is_accepted === false) Mail::to($article->user->email)->send(new RejectedArticle($article));
        return true;
    }

    public function toSearchableArray()
    {
        $category = $this->category;
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => $category,
        ];

        return $array;
        
    }

    public function images(){
        return $this->hasMany(Image::class);
    }
    
    public function users(){
        return $this->belongsToMany(User::class);
    }
}
