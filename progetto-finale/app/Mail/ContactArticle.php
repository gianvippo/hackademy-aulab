<?php

namespace App\Mail;

use App\Models\Article;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactArticle extends Mailable
{
    use Queueable, SerializesModels;

    public $name_seller, $name_buyer, $email_buyer, $text, $article;
    
    public function __construct($buyer, Article $article, $message_text)
    {
        $this->name_seller = $article->user->name;
        $this->name_buyer = $buyer->name;
        $this->email_buyer = $buyer->email;
        $this->text = $message_text;
        $this->article = $article;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: 'Nuovo messaggio da ' . $this->name_buyer,
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'mail.contact-seller',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
