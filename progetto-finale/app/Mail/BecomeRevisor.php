<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Contracts\Queue\ShouldQueue;

class BecomeRevisor extends Mailable
{
    use Queueable, SerializesModels;
    
    public $user;
    public $description;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $description)
    {
        $this->user = $user;
        $this->description = $description;
    }

    public function build()
    {
        return $this->from('noreply@presto.it')->view('mail.become_revisor');
    }
 
}
