<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {    
        $categories = [
            ["Generica", "Generic", "Genèrico", "./media/bg11.jpg"],
            ["Abbigliamento", "Clothes", "Ropa", "./media/abbigliamento.jpg"],
            ["Arredamento", "Furniture", "Mobiliario", "./media/arredamento.jpg"],
            ["Film", "Film", "Pelìculas", "./media/film1.jpg"],
            ["Immobili", "Houses", "Inmobiliario", "./media/immobili.jpg"],
            ["Informatica", "Technology", "Informàtica", "./media/informatica.jpg"],
            ["Libri", "Books", "Libros", "./media/libri.jpg"],
            ["Motori", "Motors", "Màquinas", "./media/motori.jpg"],
            ["Musica", "Music", "Mùsica", "./media/musica.jpg"],
            ["Scarpe", "Shoes", "Zapatos", "./media/scarpe.jpg"],
            ["Sport", "Sport", "Deporte", "./media/sport.jpg"]                      
        ];

        foreach($categories as $category){
            DB::table('categories')->insert([
                'nameIt'=>$category[0],
                'nameEn'=>$category[1],
                'nameEs'=>$category[2],
                'image'=>$category[3]
            ]);
        }
    }
}
