<?php

return [

//HOMEPAGE
'cta_1'=>'Compra.',
'cta_2'=>'Vendi.',
'cta_3'=>'Presto.',
'check_it_out'=>'Scopri di più',

//NAVBAR
'all_articles'=>'Tutti gli annunci',
'archive'=>'Archivio',
'cerca'=>'Cerca',
'search_articles'=>'Cerca annunci...',
'add_articles'=>'Inserisci Annuncio',
'hi'=>'Ciao',
'register'=>'Registrati',
'login'=>'Accedi',
'not_registered'=>'Non sei ancora registrato?',
'click_here'=>'Clicca qui',
'favorites'=>'Preferiti',
'my_articles'=>'I Miei Annunci',

//FOOTER
'cta_revisor'=>'Vuoi lavorare con noi?',
'cta_revisor_button'=>'Diventa revisore',
'rights_reserved'=>'Tutti i diritti sono riservati',

//MAIL BECOME REVISOR
'work_with_us'=>'Lavora con noi',
'about_you'=>'Dicci qualcosa di te',

//REGISTER FORM e LOGIN FORM
'email'=>'Inserisci la tua email',
'name'=>'Inserisci il tuo nome',
'password'=>'Inserisci la password',
'password_confirmation'=>'Conferma la password',

//ARTICLE INDEX
'no_articles'=>'Siamo spiacenti, al momento non sono presenti annunci. Riprova in un altro momento.',
'delete'=>'Elimina',

//ARTICLE SHOW
'noImages'=>'Nessuna immagine',
'author'=>'Inserito da',
'back'=>'Torna indietro',
'created'=>'Annuncio del',
'description'=>'Descrizione',
'contact'=>'Contatta il venditore',
'message'=>'Messaggio',
'agreement'=>'Sono consapevole che i miei dati quali Nome e Email verranno inviati al venditore',
'close'=>'Chiudi',
'send_message'=>'Invia il Messaggio',

//LIVEWIRE CATEGORIES
'show_categories'=>'Mostra le categorie',

//LIVEWIRE CREATEARTICLEFORM
'title'=>'Inserisci il titolo dell\'annuncio',
'category'=>'Seleziona la categoria',
'price'=>'Inserisci il prezzo',
'images'=>'Inserisci immagini',
'preview'=>'Anteprima',
'insert_description'=>'Inserisci una descrizione',
'submit'=>'Inserisci',

//LIVEWIRE VALIDATION BUTTONS
'accept'=>'Accetta',
'reject'=>'Rifiuta',
'approve'=>'Articolo approvato',

//REVISOR ARCHIVE
'accepted'=>'Accettato',
'rejected'=>'Rifiutato',
'restore'=>'Ripristina',
'photo'=>'Foto',
'title'=>'Titolo',
'date'=>'Data',
'status'=>'Stato',
'actions'=>'Azioni',
'pending'=>'In attesa',
'category'=>'Categoria',
'price'=>'Prezzo',

//REVISOR INDEX
'your_dashboard'=>'Dashboard',
'no_revisor_articles'=>'Non sono presenti articoli da revisionare',
'revisor_zone'=>'Area Revisore',

//LIVEWIRE REVISOR CARD
'adults'=>'Adulti',
'spoof'=>'Satira',
'medical'=>'Medicina',
'violence'=>'Violenza',
'racy'=>'Inadeguato',
'no_images'=>'L\'utente non ha caricato immagini',

//PROFILE
'saved_articles'=>'Articoli salvati',
'no_saved_articles'=>'Non ci sono articoli salvati nel tuo profilo',
'profile'=>'Profilo',

];

