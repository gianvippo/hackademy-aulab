<?php

return [

//HOMEPAGE
'cta_1'=>'Buy.',
'cta_2'=>'Sell.',
'cta_3'=>'Presto.',
'check_it_out'=>'Check it out',

//NAVBAR
'all_articles'=>'All articles',
'archive'=>'Archive',
'cerca'=>'Search',
'search_articles'=>'Search articles...',
'add_articles'=>'Add an article',
'hi'=>'Hi',
'register'=>'Register',
'login'=>'Login',
'not_registered'=>'Not registered yet?',
'click_here'=>'Click here',
'favorites'=>'Favorites',
'my_articles'=>'My Articles',


//FOOTER
'cta_revisor'=>'Would you like to work with us?',
'cta_revisor_button'=>'Become Revisor',
'rights_reserved'=>'All rights are reserved',

//MAIL BECOME REVISOR
'work_with_us'=>'Work with us',
'about_you'=>'Tell us something about you',

//REGISTER FORM e LOGIN FORM
'email'=>'Your email',
'name'=>'Your name',
'password'=>'Enter password',
'password_confirmation'=>'Confirm password',

//ARTICLE INDEX
'no_articles'=>'We\'re sorry, but there aren\'t articles. Try later.',
'delete'=>'Delete',

//ARTICLE SHOW
'noImages'=>'Any images existing.',
'author'=>'Created by',
'back'=>'Go back',
'created'=>'Created on',
'description'=>'Description',
'contact'=>'Contact seller',
'message_for'=>'Message for',
'message'=>'Message',
'agreement'=>'I am aware that my data such as Name and Email will be sent to the seller',
'close'=>'Close',
'send_message'=>'Send Message',

//LIVEWIRE CATEGORIES
'show_categories'=>'Show categories',

//LIVEWIRE CREATEARTICLEFORM
'title'=>'Title',
'category'=>'Choose a category',
'price'=>'Price',
'images'=>'Enter some images',
'preview'=>'Preview',
'insert_description'=>'Describe your article',
'submit'=>'Submit',

//LIVEWIRE VALIDATION BUTTONS
'accept'=>'Accept',
'reject'=>'Reject',
'approve'=>'Approved article',

//REVISOR ARCHIVE
'accepted'=>'Accepted',
'rejected'=>'Rejected',
'restore'=>'Restore',
'photo'=>'Photo',
'title'=>'Title',
'date'=>'Date',
'status'=>'Status',
'actions'=>'Actions',
'pending'=>'Pending',
'category'=>'Category',
'price'=>'Price',

//REVISOR INDEX
'your_dashboard'=>'Dashboard',
'no_revisor_articles'=>'There aren\'t articles to be revisioned',
'revisor_zone'=>'Revisor Zone',

//LIVEWIRE REVISOR CARD
'adults'=>'Adults',
'spoof'=>'Spoof',
'medical'=>'Medical',
'violence'=>'Violence',
'racy'=>'Racy',
'no_images'=>'User has not uploaded any pictures',

//PROFILE
'profile'=>'Profile',
'saved_articles'=>'Saved Articles',
'no_saved_articles'=>'There aren\'t articles saved on your profile',

];

