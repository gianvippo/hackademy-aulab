<?php

return [

//HOMEPAGE
'cta_1'=>'Compra.',
'cta_2'=>'Vende.',
'cta_3'=>'Presto.',
'check_it_out'=>'Descubre',

//NAVBAR
'all_articles'=>'Todos los anuncios',
'archive'=>'Archivo',
'cerca'=>'Busca',
'search_articles'=>'Buscar anuncios...',
'add_articles'=>'Insertar anuncio',
'hi'=>'¡Hola!',
'register'=>'Registrarse',
'login'=>'Iniciar sesiòn',
'not_registered'=>'¿No registrado?',
'click_here'=>'Haga clic aquí',
'favorites'=>'Favoritos',
'my_articles'=>'My Articles',

//FOOTER
'cta_revisor'=>'¿Quiere colaborar con nosotros?',
'cta_revisor_button'=>'Hágase auditor',
'rights_reserved'=>'Todos los derechos reservados',

//MAIL BECOME REVISOR
'work_with_us'=>'Trabaje con nosotros',
'about_you'=>'Cuéntenos algo de usted',

//REGISTER FORM e LOGIN FORM
'email'=>'Vos correo electrònico',
'name'=>'Vos nombre',
'password'=>'Vos contraseña',
'password_confirmation'=>'Confirme su contraseña',

//ARTICLE INDEX
'no_articles'=>'Lo sentimos, actualmente no hay anuncios. Vuelva a intentarlo en otro momento.',
'delete'=>'Eliminar',

//ARTICLE SHOW
'noImages'=>'No hay imàgenes',
'author'=>'Insertado por',
'back'=>'Volver atràs',
'created'=>'Anuncio del',
'description'=>'Descripciòn',
'contact'=>'Contacte al vendedor',
'message_for'=>'Mensaje por',
'message'=>'Mensaje',
'agreement'=>'Soy consciente de que mis datos, como el nombre y el correo electrónico, se enviarán al vendedor',
'close'=>'Cerrar',
'send_message'=>'Envía el Mensaje',

//LIVEWIRE CATEGORIES
'show_categories'=>'Mostrar categorìas',

//LIVEWIRE CREATEARTICLEFORM
'title'=>'Introduce el título del anuncio',
'category'=>'Seleccione una categoría',
'price'=>'Insertar precio',
'images'=>'Insertar imágenes',
'preview'=>'Previsualización de la imagen',
'insert_description'=>'Insertar una descripciòn',
'submit'=>'Insertar',

//LIVEWIRE VALIDATION BUTTONS
'accept'=>'Aceptar',
'reject'=>'Rechazar',
'approve'=>'Artìculo aprobado',

//REVISOR ARCHIVE
'accepted'=>'Aceptado',
'rejected'=>'Rechazado',
'restore'=>'Restaura',
'photo'=>'Fotos',
'title'=>'Tìtulo',
'date'=>'Fecha',
'status'=>'Situaciòn',
'actions'=>'Acciones',
'pending'=>'Pendiente',
'category'=>'Categoría',
'price'=>'Precio',

//REVISOR INDEX
'your_dashboard'=>'Panel de control',
'no_revisor_articles'=>'No hay artículos pendientes de revisión',
'revisor_zone'=>'Área del Auditor',

//LIVEWIRE REVISOR CARD
'adults'=>'Adultos',
'spoof'=>'Sátira',
'medical'=>'Medicina',
'violence'=>'Violencia',
'racy'=>'Inadecuado',
'no_images'=>'El usuario no ha subido ninguna imagen',

//PROFILE
'saved_articles'=>'Artículos Favoritos',
'no_saved_articles'=>'No hay artículos Favoritos en tu perfil',
'profile'=>'Perfil',


];

