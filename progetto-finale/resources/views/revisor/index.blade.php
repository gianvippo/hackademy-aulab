<x-layout title="{{__('ui.your_dashboard')}} | presto.it">
    @livewire('categories-navbar')
    <x-header>{{__('ui.your_dashboard')}}</x-header>

    <div class="container">
        <x-session-messages />
        <div class="row justify-content-center">

            @forelse ($articles_to_check as $article)
            @livewire('revisor-card', compact('article'))

            @empty
            <div class="col-12 text-center mt-5 pt-5">
                <p class="display-5 dark-brown fw-bold text-uppercase font-poppins mt-5">{{__('ui.no_revisor_articles')}}</p>
            </div>
            @endforelse

            {{ $articles_to_check->links() }}

        </div>
    </div>


</x-layout>