<x-layout title="{{__('ui.archive')}} | presto.it">
    
    @livewire('categories-navbar')
    
    <x-header>{{__('ui.archive')}}</x-header>
    
    <div class="container rev-shadow mt-5 px-4 p-md-3">
        <x-session-messages />    
        
        <div class="row border-archive slimy-green py-1">
            <div class="col d-flex justify-content-around">
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.photo')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.title')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.category')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.author')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.date')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.status')}}</p>
                <p class="d-none d-lg-block dark-green font-fredoka">{{__('ui.actions')}}</p>
            </div>
        </div>
        @forelse ($articles_checked as $article)
        <div class="row py-2 border-archive archive">
            <div class="col-4 col-md align"> 
                @if (count($article->images))
                <img src="{{$article->images()->first()->getUrl(400,400)}}" alt="@if($article->images()->first()->labels)@foreach($article->images()->first()->labels as $key => $label)@if($key === array_key_last($article->images()->first()->labels)){{$label}}@else{{$label}},@endif @endforeach @endif">
                @endif
            </div>
            
            <div class="col-4 col-md align justify-content-start text-center text-md-start">
                <a href="{{route('article.show', compact('article'))}}"
                class="text-decoration-none font-poppins">{{$article->title}}</a>
            </div>
            
            @switch(app()->getLocale())
            @case('it')
            <div class="col d-none d-md-flex align font-poppins fst-italic small">{{$article->category->nameIt}}</div>
            @break
            @case('es')
            <div class="col d-none d-md-flex align font-poppins fst-italic small">{{$article->category->nameEs}}</div>
            @break
            @default
            <div class="col d-none d-md-flex align font-poppins fst-italic small">{{$article->category->nameEn}}</div>
            @endswitch
            
            
            <div class="col d-none d-md-flex align font-poppins">{{$article->user->name}}</div>
            
            <div class="col d-none d-md-flex align font-poppins">{{$article->created_at->format('d/m/Y')}}</div>
            
            
            @if ($article->is_accepted === NULL)
            <div class="col-4 col-md align text-warning fw-bold font-poppins">{{__('ui.pending')}}</div>
            <div class="col-md-12 col-lg my-2 m-md-0 d-flex flex-lg-column justify-content-center justify-content-lg-start">
                <form action="{{route('revisor.accept_article', compact('article'))}}" class="d-flex align-items-center" method="POST">
                    @csrf
                    @method('PATCH')
                    <button type="submit" class="border-0 bg-transparent d-flex">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                        width="48" height="48"
                        viewBox="0 0 48 48">
                        <circle cx="24" cy="24" r="20" fill="#3ddab4"></circle>
                        <polygon fill="#fff" points="39.091,17.515 35.015,13.604 22.775,26.233 16.679,20.433 12.768,24.509 18.843,30.289 18.843,30.289 22.92,34.179 22.926,34.173 22.932,34.179 26.86,30.119 26.86,30.119"></polygon><polygon fill="#fff" points="22.926,34.173 26.86,30.119 22.775,26.233 18.843,30.289"></polygon>
                    </svg>
                    <p class="m-auto font-poppins small text-muted">{{__('ui.accept')}}</p>
                </button>
            </form>
            <form action="{{route('revisor.reject_article', compact('article'))}}" class="d-flex align-items-center" method="POST">
                @csrf
                @method('PATCH')
                <button type="submit" class="border-0 bg-transparent d-flex">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                    width="48" height="48"
                    viewBox="0 0 48 48">
                    <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
                </svg>
                <p class="my-auto font-poppins small text-muted">{{__('ui.reject')}}</p>
            </button>
        </form>
    </div>
    @endif
    
    @if ($article->is_accepted)
    <div class="col-4 col-md align text-success fw-bold font-poppins">{{__('ui.accepted')}}</div>
    <div class="col-md-12 col-lg my-2 m-md-0 d-flex align-items-center justify-content-center justify-content-lg-start">
        <form action="{{route('revisor.reject_article', compact('article'))}}"  class="d-flex" method="POST">
            @csrf
            @method('PATCH')
            <button type="submit" class="border-0 bg-transparent d-flex">
                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                width="48" height="48"
                viewBox="0 0 48 48">
                <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
            </svg>
            <p class="my-auto font-poppins small text-muted">{{__('ui.reject')}}</p>
        </button>
    </form>
</div>
@endif

@if ($article->is_accepted === 0)
<div class="col-4 col-md align text-danger fw-bold font-poppins">{{__('ui.rejected')}}</div>
<div class="col-md-12 col-lg my-2 m-md-0 d-flex align-items-center justify-content-center justify-content-lg-start">
    <form action="{{route('revisor.cancel_action', compact('article'))}}" class="d-flex" method="POST">
        @csrf
        @method('PATCH')
        <button type="submit" class="border-0 bg-transparent d-flex">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
            width="48" height="48"
            viewBox="0,0,256,256"
            style="fill:#000000;">
            <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><g transform="scale(5.33333,5.33333)"><path d="M24,3c-11.59798,0 -21,9.40202 -21,21c0,11.59798 9.40202,21 21,21c11.59798,0 21,-9.40202 21,-21c0,-11.59798 -9.40202,-21 -21,-21z" fill="#fcc419"></path><path d="M24,36.1c-6.6,0 -12,-5.4 -12,-12c0,-3.6 1.6,-7 4.4,-9.3l2.5,3.1c-1.8,1.5 -2.9,3.8 -2.9,6.2c0,4.4 3.6,8 8,8c4.4,0 8,-3.6 8,-8c0,-2.1 -0.8,-4 -2.2,-5.5l2.9,-2.7c2.1,2.1 3.3,5.1 3.3,8.2c0,6.6 -5.4,12 -12,12z" fill="#fefffd"></path><path d="M12,13h9v9z" fill="#fefffd"></path></g></g>
        </svg>
        <p class="my-auto font-poppins small text-muted">{{__('ui.restore')}}</p>
    </button>
</form>
</div>
@endif
</div>
@empty
<div class="row">
    <div class="col-12"> 
        <p class="display-5 font-poppins">{{__('ui.no_articles')}}</p>
    </div>
</div>
@endforelse
</div>


</x-layout>