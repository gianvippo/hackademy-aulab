<x-layout title="{{__('ui.my_articles')}} | presto.it">

    @livewire('categories-navbar')

    <x-header>{{__('ui.my_articles')}}</x-header>

    <div class="container mt-5 px-4 px-md-0">
        <x-session-messages />

        <div class="row border-archive slimy-green py-1">
            <div class="col d-flex justify-content-around">
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.photo')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.title')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.category')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.date')}}</p>
                <p class="d-none d-md-block dark-green font-fredoka">{{__('ui.status')}}</p>
                <p class="d-none d-lg-block dark-green font-fredoka">{{__('ui.actions')}}</p>
            </div>
        </div>
        @forelse ($articles as $article)
        <div class="row py-2 border-archive archive">
            <div class="col-4 col-md align">
                @if (count($article->images))
                <img src="{{$article->images()->first()->getUrl(400,400)}}" alt="@if($article->images()->first()->labels)@foreach($article->images()->first()->labels as $key => $label)@if($key === array_key_last($article->images()->first()->labels)){{$label}}@else{{$label}},@endif @endforeach @endif">
                @endif
            </div>

            <div class="col-4 col-md align justify-content-start text-center text-md-start">
                <a href="{{route('article.show', compact('article'))}}"
                    class="text-decoration-none font-poppins">{{$article->title}}</a>
            </div>

            @switch(app()->getLocale())
            @case('it')
            <div class="col d-none d-md-flex align font-poppins fst-italic small">{{$article->category->nameIt}}</div>
            @break
            @case('es')
            <div class="col d-none d-md-flex align font-poppins fst-italic small">{{$article->category->nameEs}}</div>
            @break
            @default
            <div class="col d-none d-md-flex align font-poppins fst-italic small">{{$article->category->nameEn}}</div>
            @endswitch

            <div class="col d-none d-md-flex align font-poppins">{{$article->created_at->format('d/m/Y')}}</div>

            @if ($article->is_accepted === NULL)
            <div class="col-4 col-md align text-warning fw-bold font-poppins">{{__('ui.pending')}}</div>
            @endif

            @if ($article->is_accepted)
            <div class="col-4 col-md align text-success fw-bold font-poppins">{{__('ui.accepted')}}</div>
            @endif

            @if ($article->is_accepted === 0)
            <div class="col-4 col-md align text-danger fw-bold font-poppins">{{__('ui.rejected')}}</div>
            @endif

            <div class="col-md-12 col-lg my-2 m-md-0 d-flex align-items-center justify-content-center justify-content-lg-start">
                <form method="POST" action="{{route('deleteArticle', compact('article'))}}" class="d-flex">
                    @csrf
                    @method('delete')
                    <button type="submit" class="border-0 bg-transparent d-flex">
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="48" height="48"
                            viewBox="0 0 48 48">
                            <path fill="#f44336"
                                d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path>
                            <path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z">
                            </path>
                            <path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z">
                            </path>
                        </svg>
                        <p class="my-auto font-poppins small text-muted">{{__('ui.delete')}}</p>
                    </button>
                </form>
            </div>
        </div>
        @empty
        <div class="row">
            <div class="col-12">
                <p class="display-5 font-poppins">{{__('ui.no_articles')}}</p>
            </div>
        </div>
        @endforelse
    </div>


</x-layout>