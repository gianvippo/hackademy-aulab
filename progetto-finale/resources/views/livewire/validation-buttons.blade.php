<div class="" id="form_{{$article->id}}">
    @auth
    @if (Auth::user()->is_revisor && $article->is_accepted == null)

    {{-- BOTTONE ACCETTA --}}
    <div class="d-flex  @if(Route::is('revisor.index')) mb-4 @endif">
        <button wire:click="revisorChoice(true)" class="border-0 bg-transparent">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
            width="58" height="58"
            viewBox="0 0 48 48">
            <circle cx="24" cy="24" r="20" fill="#3ddab4"></circle>
            <polygon fill="#fff" points="39.091,17.515 35.015,13.604 22.775,26.233 16.679,20.433 12.768,24.509 18.843,30.289 18.843,30.289 22.92,34.179 22.926,34.173 22.932,34.179 26.86,30.119 26.86,30.119"></polygon><polygon fill="#fff" points="22.926,34.173 26.86,30.119 22.775,26.233 18.843,30.289"></polygon>
            </svg>              
        </button>
        <p class="my-auto font-poppins">{{__('ui.accept')}}</p>
    </div>

    {{-- BOTTONE RIFIUTA --}}
    <div class="d-flex  @if(Route::is('revisor.index')) mt-4 @endif">
        <button wire:click="revisorChoice(false)" class="border-0 bg-transparent">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
            width="58" height="58"
            viewBox="0 0 48 48">
            <path fill="#f44336" d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path><path fill="#fff" d="M29.656,15.516l2.828,2.828l-14.14,14.14l-2.828-2.828L29.656,15.516z"></path><path fill="#fff" d="M32.484,29.656l-2.828,2.828l-14.14-14.14l2.828-2.828L32.484,29.656z"></path>
            </svg>
        </button>
        <p class="my-auto font-poppins">{{__('ui.reject')}}</p>
    </div>

    <script>
        function cleanForm(id, bool, lang) {
            let container = document.querySelector(`#form_${id}`)
            container.innerHTML = ""
            let approved
            let rejected

            switch (lang) {
                case 'it':
                    approved = `<div class="alert alert-success">
                                Articolo approvato
                                </div>`
                    rejected = `<div class="alert alert-danger">
                                Articolo non approvato
                                </div>`
                    break;
                
                case 'es':
                    approved = `<div class="alert alert-success">
                                Anuncio aceptado
                                </div>`
                    rejected = `<div class="alert alert-danger">
                                Anuncio no aceptado
                                </div>`
                    break;
            
                default:
                    approved = `<div class="alert alert-success">
                                Accepted announcement
                                </div>`
                    rejected = `<div class="alert alert-danger">
                                Rejected announcement
                                </div>`
                    break;
            }

            if (bool) container.innerHTML = approved
            else container.innerHTML = rejected

        }

        function showCancel(id, bool, lang) {

            let container = document.querySelector(`#form_${id}`)

            container.innerHTML = ""
            let form = document.createElement('form')
            form.action = `{{route('revisor.cancel_action', compact('article'))}}`
            form.method = "POST"
            form.innerHTML = `@csrf @method('PATCH')`
            container.appendChild(form)
            let button = document.createElement('button')
            button.type = "submit"
            button.classList.add("btn", "btn-primary", "cancel-btn", "fs-4", "mx-auto", "d-flex", "p-3")
            let button_text
            let form_text
            switch (lang) {
                case 'it':
                    button_text = "Annulla"
                    form_text = `<div class="fst-italic fs-5 text-center">Premi Annulla se vuoi ripristinare l'annuncio</div>`
                    break;
                
                case 'es':
                    button_text = "Cancelar"
                    form_text = `<div class="fst-italic fs-5 text-center">Presione Cancelar si desea restaurar el anuncio</div>`
                    break;
            
                default:
                    button_text = "Cancel"
                    form_text = `<div class="fst-italic fs-5 text-center">Click Cancel to restore the announcement</div>`
                    break;
            }

            button.innerHTML = button_text
            form.appendChild(button)
            
            form.innerHTML += form_text

            setTimeout(cleanForm, 5000, id, bool, lang)

        }

        document.addEventListener('choiceAd', (item) => {

            let id = item.detail.id
            let bool = item.detail.bool
            let lang = item.detail.lang

            showCancel(id, bool, lang)

        })


    </script>
    @endif 
    @endauth  
</div>