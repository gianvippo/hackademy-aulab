<nav id="categories-nav" class="navbar navbar-expand-md">
    <div class="container-fluid">
        <button class="navbar-toggler button-custom-small mx-auto text-light" type="button" data-bs-toggle="collapse" data-bs-target="#categories" aria-controls="categories" aria-expanded="false" aria-label="Toggle navigation">
            {{__('ui.show_categories')}}
        </button>
        <div class="collapse navbar-collapse justify-content-around" id="categories">
            <div class="row">
                <div class="col-12 d-xl-flex text-center">
                    <a class="text-decoration-none text-reset btn font-poppins rounded-0" href="{{route('article.index')}}">{{__('ui.all_articles')}}</a>
                    @foreach ($categories as $category)
                    <a class="text-decoration-none text-reset btn font-poppins rounded-0" aria-current="page" href="{{route('article.indexByCategory', compact('category'))}}">
                    @switch(app()->getLocale())
                        @case('it')
                            {{$category->nameIt}}
                            @break
                        @case('es')
                        {{$category->nameEs}}
                            @break
                        @default
                        {{$category->nameEn}}  
                    @endswitch
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</nav>