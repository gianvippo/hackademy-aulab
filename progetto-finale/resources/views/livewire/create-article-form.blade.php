<div class="container my-5 p-4">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <form class="rev-shadow lead p-4 bg-presto-light" wire:submit.prevent="store" enctype="multipart/form-data">
                @csrf
                <x-session-messages />

                <div class="mb-4">
                    <label for="title" class="form-label fs-4">{{__('ui.title')}}</label>
                    <input wire:model.lazy='title' type="text" class="form-control rounded-0" id="title">
                    @error('title')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="category" class="form-label fs-4">{{__('ui.category')}}</label>
                    <select wire:model="category" id="category" class="form-select rounded-0">
                        @foreach ($categories as $item)
                        <option value="{{$item->id}}">
                            @switch(app()->getLocale())
                            @case('it')
                            {{$item->nameIt}}
                            @break
                            @case('es')
                            {{$item->nameEs}}
                            @break
                            @default
                            {{$item->nameEn}}
                            @endswitch
                        </option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-4">
                    <label for="price" class="form-label fs-4">{{__('ui.price')}}</label>   
                    <input wire:model.lazy='price' class="form-control rounded-0" id="price" placeholder="€">
                    
                    @error('price')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="temporary_images" class="form-label fs-4">{{__('ui.images')}}</label>
                    <input wire:model="temporary_images" type="file" id="temporary_images" placeholder="Img" multiple
                        class="form-control rounded-0" />
                    @error('temporary_images.*')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                    @enderror
                    @error('temporary_images')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                    @enderror
                </div>
                @if(!empty($images))
                <div class="row">
                    <div class="col-12">
                        <p>{{__('ui.preview')}}</p>
                        <div class="row">
                            @foreach($images as $key => $image)
                            <div class="col">
                                <a href="#" wire:click="removeImage({{$key}})">
                                    <div class="anteprima"
                                        style="background-image: url({{$image->temporaryUrl()}});">
                                        <div class="delete-img"></div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif

                <div class="mb-4">
                    <label for="description" class="form-label fs-4">{{__('ui.insert_description')}}</label>
                    <textarea wire:model.lazy="description" class="form-control rounded-0" id="description"
                        rows="7"></textarea>
                    @error('description')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                    @enderror
                </div>

                <div class="text-center">
                    <button type="submit" onclick="window.scrollTo(0, 0);"
                        class="button-custom">{{__('ui.submit')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>