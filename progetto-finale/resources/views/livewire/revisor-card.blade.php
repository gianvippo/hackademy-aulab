<div class="container-fluid bg-presto-light rev-shadow my-4">
    @if (count($article->images))
    <div class="row">
        @foreach ($article->images as $image)
        <div class="col-12 col-md-3">
            <img src="{{$image->getUrl(400,400)}}" class="w-100 mt-2" alt="@if($image->labels)@foreach($image->labels as $key => $label)@if($key === array_key_last($image->labels)){{$label}}@else{{$label}},@endif @endforeach @endif">

            <details class="card mb-2">
                <summary class="card-header d-flex justify-content-center">
                    <span class="{{$this->check($image)}}"></span>
                </summary>
                <div class="row d-flex justify-content-center">
                    <div class="col-6">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{__('ui.adults')}}: <span class="{{$image->adult}}"></span></li>
                            <li class="list-group-item">{{__('ui.spoof')}}: <span class="{{$image->spoof}}"></span></li>
                            <li class="list-group-item">{{__('ui.medical')}}: <span class="{{$image->medical}}"></span></li>
                            <li></li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{__('ui.violence')}}: <span class="{{$image->violence}}"></span></li>
                            <li class="list-group-item">{{__('ui.racy')}}: <span class="{{$image->racy}}"></span></li>
                            <li></li>

                        </ul>
                    </div>
                </div>

                @if ($image->labels)
                <div class="p-2">
                    <p class="m-0"><strong>Labels</strong></p>
                    @foreach ($image->labels as $key => $label)
                    @if ($key === array_key_last($image->labels))
                    <p class="d-inline">{{$label}}</p>
                    @else
                    <p class="d-inline">{{$label}},</p>
                    @endif
                    @endforeach
                </div>
                @endif
            </details>
        </div>
        @endforeach
    </div>
    @else
    <div class="row justify-content-center">
        <div class="col-12">
            <p class="display-5 dark-brown mt-3">{{__('ui.no_images')}}</p>
        </div>
    </div>
    @endif
    <div class="row rev-pannel mt-2 text-white">
        <div class="col-12 col-md-6 col-lg-4">
            <div class="row">
                <div class="col-12 mt-2">
                    <i>{{__('ui.title')}}</i>
                    <h4 class="fw-bold font-poppins mt-1 mb-3">{{$article->title}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-6"><i>{{__('ui.price')}}</i>
                    <h5 class="fw-bold font-poppins mt-1 mb-3">{{$article->price}} &euro;</h5>
                </div>
                <div class="col-6"><i>{{__('ui.category')}}</i>
                    <h5 class="fw-bold font-poppins mt-1 mb-3">{{$article->category->nameIt}}</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6"><i>{{__('ui.author')}}</i>
                    <h5 class="fw-bold mt-1 mb-3 font-poppins"> {{$article->user->name}}</h5>
                </div>
                <div class="col-12 col-md-6"><i>{{__('ui.date')}}</i>
                    <h5 class="fw-bold font-poppins mt-1 mb-3">{{$article->created_at->format('d/m/Y')}}</h5>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 mt-2">
            <i>{{__('ui.description')}}</i>
            <p class="fw-bold font-poppins mt-2 mb-3">{{$article->description}}</p>
        </div>
        <div class="col-12 col-md-12 col-lg-2 d-flex align-items-center justify-content-center">
            @livewire('validation-buttons', compact('article'))
        </div>
    </div>
</div>