<div class="col-12 col-md-6 col-lg-3 py-3 px-1">
    <a href="{{route('article.show', compact('article'))}}" style="background-image: url({{count($article->images) ? $article->images()->first()->getUrl(400,400) : '/media/logocarrello.png'}})" class="card position-relative scheda text-decoration-none mx-3 rounded-0">
        <p class="card-title position-absolute bottom-0 mb-0 titolo-scheda fs-6 fw-bold">{{$article->title}}
        <span class="text-warning">{{number_format($article->price, 0, ',', '.')}} &euro;</span></p> 
    </a>
</div>
