<div>
    <a href="{{url()->previous() == url()->current() ? route('article.index') : url()->previous() }}"
        class="button-custom font-fredoka" data-bs-toggle="modal" data-bs-target="#modalForm">{{__('ui.contact')}}</a>

    <div wire:ignore.self class="modal fade" id="modalForm" tabindex="-1" aria-labelledby="modalFormLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="modalFormLabel">{{__('ui.message_for')}} {{$article->user->name}}</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form wire:submit.prevent="send">
                    <div class="modal-body">
                        @csrf
                        <div class="d-none" id="sendMsg"></div>
                        <div class="mb-3">
                            @error('message_text')
                            <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}
                            </div>
                            @enderror
                            <label for="message_text" class="col-form-label">{{__('ui.message')}}:</label>
                            <textarea class="form-control" id="message_text" wire:model.lazy="message_text"
                                rows="6"></textarea>
                        </div>
                        <div class="form-check">

                            <input class="form-check-input" type="checkbox" value="" id="checkbox"
                                wire:model.lazy="checkbox">
                            <label class="form-check-label fst-italic" for="checkbox">
                                {{__('ui.agreement')}}
                                {{$article->user->name}}
                            </label>
                            @error('checkbox')
                            <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('ui.close')}}</button>
                        <button type="submit" class="btn btn-primary" id="btnSubmit">{{__('ui.send_message')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function dismissModal(message) {

            message.classList.remove('msg-send')
            $('#modalForm').modal('hide');

        }

        document.addEventListener('msgSend', (item) => {

            let lang = item.detail.lang

            let message = document.querySelector('#sendMsg')
            message.classList.remove('d-none')
            message.classList.add('alert', 'alert-success', 'text-center', 'rounded-0', 'msg-send')

            switch (lang) {
                case 'it':
                    message.innerHTML = 'Il messaggio è stato inviato al venditore, il quale ti risponderà al più presto'
                    break;

                case 'es':
                    message.innerHTML = 'Su mensaje ha sido enviado al vendedor, quien se pondrá en contacto con usted lo antes posible'
                    break;
            
                default:
                    message.innerHTML = 'Your message has been sent to the seller, who will get back to you as soon as possible'
                    break;
            }
            

            let message_text = document.querySelector('#message_text')
            message_text.setAttribute('disabled', '')

            let checkbox = document.querySelector('#checkbox')
            checkbox.setAttribute('disabled', '')

            let btnSubmit = document.querySelector('#btnSubmit')
            btnSubmit.setAttribute('disabled', '')

            setTimeout(dismissModal, 3000, message)

        })
    </script>
</div>