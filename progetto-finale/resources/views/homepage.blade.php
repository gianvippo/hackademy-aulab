<x-layout title="presto.it">
    
    <div class="container-fluid">
        <x-session-messages />   
    </div>
    
  <div id="main-homepage" class="container-fluid py-5 my-5 d-lg-none">
        <div class="row justify-content-center align-items-center text-center py-5 my-5">
            <div class="col-12 col-md-6 py-5 my-5">
                <h1 class="display-1 text-uppercase font-fredoka dark-green">Presto.it</h1>
                <div class="subtitles d-flex justify-content-center">
                    <p class="fs-4 fw-bold slimy-green" data-aos="fade-up" data-aos-duration="800">{{__('ui.cta_1')}}</p>
                    <p class="fs-4 fw-bold slimy-green mx-5" data-aos="fade-up" data-aos-delay="400" data-aos-duration="800">{{__('ui.cta_2')}}</p>
                    <p class="fs-4 fw-bold slimy-green" data-aos="fade-up" data-aos-delay="700" data-aos-duration="800">{{__('ui.cta_3')}}</p>
                </div>    
                <div class="shop-now my-5 ">
                    <a href="{{route('article.index')}}" class="cta text-decoration-none">
                        <span class="hover-underline-animation fs-4">{{__('ui.check_it_out')}}</span>
                        <svg viewBox="0 0 46 16" height="10" width="30" xmlns="http://www.w3.org/2000/svg" id="arrow-horizontal">
                            <path transform="translate(30)" d="M8,0,6.545,1.455l5.506,5.506H-30V9.039H12.052L6.545,14.545,8,16l8-8Z" data-name="Path 10" id="Path_10"></path>
                        </svg>
                    </a>  
                </div>  
            </div>
        </div>
    </div>  

    <div id="head" class="d-none d-lg-block">
        <video playsinline="playsinline" autoplay="autoplay" muted="muted">
          <source src="./media/homepagevideo.mp4" type="video/mp4">
        </video>
        <div class="container h-100">
          <div class="d-flex h-100 text-center align-items-end">
            <div class="w-100 text-white">
                <div class="shop-now mb-5 pb-5">
                    <a href="{{route('article.index')}}" class="cta text-decoration-none">
                        <span class="hover-underline-animation fs-1">{{__('ui.check_it_out')}}</span>
                        <svg viewBox="0 0 46 16" height="10" width="30" xmlns="http://www.w3.org/2000/svg" id="arrow-horizontal">
                            <path transform="translate(30)" d="M8,0,6.545,1.455l5.506,5.506H-30V9.039H12.052L6.545,14.545,8,16l8-8Z" data-name="Path 10" id="Path_10"></path>
                        </svg>
                    </a>  
                </div>  
            </div>
          </div>
        </div>
      </div>
    
</x-layout>