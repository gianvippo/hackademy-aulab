<x-layout title="{{__('ui.work_with_us')}} | presto.it">
  
  @livewire('categories-navbar')
  <x-header>
    {{__('ui.work_with_us')}} 
  </x-header>
  
  <x-session-messages />
  
  <section class="text-center text-lg-start">     
    <div class="container py-4">
      <div class="row g-0 align-items-center">
        <div class="col-lg-6 mb-5 mb-lg-0">
          <div class="card cascading-right" style="
          background: hsla(0, 0%, 100%, 0.55);
          backdrop-filter: blur(30px);
          ">
          <div class="card-body p-5 shadow-5 text-center">
            <form method='POST' action='{{route('become_revisor')}}'>
              @csrf
              <div class="row justify-content-center">
                <div class="col-md-8 mb-4">
                  <div class="form-outline my-4">
                    <label for="description" class="form-label dark-green font-poppins text-uppercase fw-bold fs-3 mb-3">{{__('ui.about_you')}}</label>
                    <textarea name="description" id="description" class="form-control rounded-0" rows="7" value="{{old('description')}}"></textarea>
                    @error('text')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i>{{$message}}</div>
                    @enderror
                  </div>
                </div>
              </div>
              <!-- Submit button -->
              <button type="submit" href="{{route('become_revisor')}}" class="button-custom d-block mx-auto font-poppins">{{__('ui.cta_revisor_button')}}</button>
              <div class="text-start">
                <a href="{{route('homepage')}}" class="button-custom-redirect mt-5 font-poppins">{{__('ui.back')}}</a>
              </div>
            </form>
          </div>
        </div>
      </div>
      
      <div class="col-lg-6 mb-5 mb-lg-0">
        <img src="/media/revisor.jpg" class="w-100 rounded-4 shadow-4"
        alt="" />
      </div>
    </div>
  </div>
</section>



</x-layout>

