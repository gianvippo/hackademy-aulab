<x-layout title="{{__('ui.favorites')}} | presto.it">

    <x-session-messages />
    
    @livewire('categories-navbar')

    <x-header>{{__('ui.saved_articles')}}</x-header>

    <div class="container mt-5">
        <div class="row justify-content-center">            
            @forelse ($articles as $article)
            @livewire('article-card', compact('article'))
            @empty
            <div class="col-12 col-md-8 text-center">
                <p class="display-5 my-5 py-5 text-uppercase font-poppins fw-bold dark-brown">{{__('ui.no_saved_articles')}}</p> 
            </div>
            @endforelse
        </div>
    </div>
    
</x-layout>

