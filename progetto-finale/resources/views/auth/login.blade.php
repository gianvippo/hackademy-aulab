<x-layout title="{{__('ui.login')}} | presto.it">
  
  <x-header>
    {{__('ui.login')}}
  </x-header>
  
  <x-session-messages />
  
  <section class="text-center text-lg-start">
    <style>
      .cascading-right {
        margin-right: -50px;
      }
      
      @media (max-width: 991.98px) {
        .cascading-right {
          margin-right: 0;
        }
      }
    </style>
    
    <div class="container py-4">
      <div class="row g-0 align-items-center">
        <div class="col-lg-6 mb-5 mb-lg-0">
          <div class="card cascading-right" style="
          background: hsla(0, 0%, 100%, 0.55);
          backdrop-filter: blur(30px);
          ">
          <div class="card-body p-5 shadow-5 text-center">
            <h2 class="fw-bold dark-green mb-5">{{__('ui.login')}}</h2>
            <form method='POST' action="{{route('login')}}">
              @csrf
              <div class="row justify-content-center">
                
                <div class="col-md-8 mb-4">
                  <div class="form-outline">
                    <label for="email" class="form-label dark-green fs-5">{{__('ui.email')}}</label>
                    <input name='email' type="email" class="form-control rounded-0" id="email" value="{{old('email')}}">
                    @error('email')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="row justify-content-center">
                <div class="col-md-8 mb-4">
                  <div class="form-outline">
                    <label for="password" class="form-label dark-green fs-5">{{__('ui.password')}}</label>
                    <input name='password' type="password" class="form-control rounded-0" id="password">
                    @error('password')
                    <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                    @enderror
                  </div>
                </div>
              </div>
              <div class="d-flex mt-3 justify-content-center">
                <p class="dark-green font-poppins me-1">{{__('ui.not_registered')}}</p>
                <a href="{{route('register')}}" class="dark-green font-poppins fw-bold">{{__('ui.click_here')}}</a>
              </div>
              <!-- Submit button -->
              <button type="submit" class="btn btn-block my-4 button-custom">
                {{__('ui.login')}}
              </button>
              
            </form>
          </div>
        </div>
      </div>
      
      <div class="col-lg-6 mb-5 mb-lg-0">
        <img src="/media/imglogin.jpg" class="w-100 rounded-4 shadow-4"
        alt="" />
      </div>
    </div>
  </div>
</section>

</x-layout>