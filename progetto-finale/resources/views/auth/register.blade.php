<x-layout title="{{__('ui.register')}} | presto.it">
    
    <x-header>
      {{__('ui.register')}}
    </x-header>
    
  <section class="text-center text-lg-start">
        <style>
          .cascading-right {
            margin-right: -50px;
          }
      
          @media (max-width: 991.98px) {
            .cascading-right {
              margin-right: 0;
            }
          }
        </style>
      
        <!-- Jumbotron -->
        <div class="container py-4">
          <div class="row g-0 align-items-center">
            <div class="col-lg-6 mb-5 mb-lg-0">
              <div class="card cascading-right" style="
                  background: hsla(0, 0%, 100%, 0.55);
                  backdrop-filter: blur(30px);
                  ">
                <div class="card-body p-5 shadow-5 text-center">
                  <h2 class="fw-bold dark-green mb-5">{{__('ui.register')}}</h2>
                  <form method='POST' action="{{route('register')}}">
                    @csrf
                    <!-- 2 column grid layout with text inputs for the first and last names -->
                    <div class="row justify-content-center">
      
                      <div class="col-md-8 mb-4">
                        <div class="form-outline">
                          <!-- <input type="text" id="form3Example1" class="form-control" />
                          <label class="form-label" for="form3Example1">First name</label> -->
                          <label for="email" class="form-label dark-green fs-5">{{__('ui.email')}}</label>
                              <input name='email' type="email" class="form-control rounded-0" id="email" value="{{old('email')}}">
                              @error('email')
                              <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                              @enderror
                        </div>
                      </div>
                    </div>
      
                    <div class="row justify-content-center">
                      <div class="col-md-8 mb-4">
                        <div class="form-outline">
                          <!-- <input type="text" id="form3Example2" class="form-control" />
                          <label class="form-label" for="form3Example2">Last name</label> -->
                          <label for="name" class="form-label dark-green fs-5">{{__('ui.name')}}</label>
                              <input name='name' type="text" class="form-control rounded-0" id="name" value="{{old('name')}}">
                              @error('name')
                              <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                              @enderror
                        </div>
                      </div>
                    </div>
      
                    <div class="row justify-content-center">
                      <div class="col-md-8 mb-4">
                        <div class="form-outline">
                          <!-- <input type="text" id="form3Example2" class="form-control" />
                          <label class="form-label" for="form3Example2">Last name</label> -->
                          <label for="password" class="form-label dark-green fs-5">{{__('ui.password')}}</label>
                              <input name='password' type="password" class="form-control rounded-0" id="password">
                              @error('password')
                              <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                              @enderror
                        </div>
                      </div>
                    </div>
      
                    <div class="row justify-content-center">
                      <div class="col-md-8 mb-4">
                        <div class="form-outline">
                          <!-- <input type="text" id="form3Example2" class="form-control" />
                          <label class="form-label" for="form3Example2">Last name</label> -->
                          <label for="password_confirmation" class="form-label dark-green fs-5">{{__('ui.password_confirmation')}}</label>
                              <input name='password_confirmation' type="password" class="form-control rounded-0" id="password_confirmation">
                              @error('password_confirmation')
                              <div class="text-danger p-2"><i class="fa-solid fa-triangle-exclamation"></i> {{ $message }}</div>
                              @enderror
                        </div>
                      </div>
                    </div>
      
                    
      
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-block my-4 button-custom">
                      {{__('ui.register')}}
                    </button>
      
                    <!-- Register buttons -->
                    
                  </form>
                </div>
              </div>
            </div>
      
            <div class="col-lg-6 mb-5 mb-lg-0">
              <img src="/media/imglogin.jpg" class="w-100 rounded-4 shadow-4"
                alt="" />
            </div>
          </div>
        </div>
      </section>
</x-layout>