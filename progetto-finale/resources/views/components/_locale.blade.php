<form action="{{route('set_language_locale', $lang)}}" method="POST">
    @csrf
    <button type="submit" class="btn d-flex">
        <img src="{{asset('vendor/blade-flags/language-' . $lang . '.svg')}}" width="32" height="32">
        @switch($lang)
            @case('it')
                <div class="ms-3 my-auto">Italiano</div>
                @break
            @case('es')
                <div class="ms-3 my-auto">Español</div>
                @break
            @default
               <div class="ms-3 my-auto">English</div> 
        @endswitch
    </button>
</form>