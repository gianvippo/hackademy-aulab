<!-- Footer -->
<footer class="text-center text-lg-start font-fredoka text-uppercase text-muted mt-5 pt-4">
     <div class="container-fluid text-center text-md-start mt-5">
          <div class="row p-4 justify-content-around align-items-center">
               @if (Auth::guest() || (Auth::check() && !Auth::user()->is_revisor && !Auth::user()->is_admin))
               <div class="col-12 col-md-3 mb-2 mb-md-0">
                    <p class="mt-4 text-muted small font-fredoka text-uppercase">© 2023 {{__('ui.rights_reserved')}}</p>
               </div>
               <div class="col-12 col-md-5 col-lg-6 text-center mb-3 mb-md-0">
                    <img src="/media/logocarrello.png" width="150px">
               </div>     
               <div class="col-12 col-md-4 col-lg-3">
                         <ul class="d-flex flex-column justify-content-center text-center">
                              <p class="text-muted small font-fredoka text-uppercase mb-2">{{__('ui.cta_revisor')}}</p>
                              <a href="{{route('work_with_us')}}" class="button-custom d-block mx-auto">{{__('ui.cta_revisor_button')}}</a> 
                         </ul>
                    </div>
               @else
                    <div class="col-12 col-md-6 mb-3 mb-md-0">
                         <img src="/media/logocarrello.png" width="150px">
                    </div>
                    <div class="col-12 col-md-6 text-center text-md-end">
                         <p class="mt-4 text-muted small font-fredoka text-uppercase">© 2023 {{__('ui.rights_reserved')}}</p>
                    </div>
               @endif
          </div>
     </div>
</footer>


