<header id="header-main" class="d-flex align-items-center justify-content-center" @isset($background)
    style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.1) 0%, rgba(255, 255, 255, 0.1) 50%, rgba(0, 0, 0, 0.1) 100%), url('{{$background}}')"
@endisset)>
    <h1 class="display-4 fw-bold p-3 header-text text-uppercase rounded-4" data-aos="fade-right" data-aos-duration="1300">{{$slot}}</h1>
</header>

