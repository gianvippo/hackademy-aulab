@if (session('successMsg'))
    <div class="alert alert-success text-center rounded-0">
        {{session('successMsg')}}
    </div>
@endif

@if (session('dangerMsg'))
    <div class="alert alert-danger text-center rounded-0">
        {{session('dangerMsg')}}
    </div>
@endif