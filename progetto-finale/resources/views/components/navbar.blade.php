<nav class="navbar navbar-expand-lg bg-transparent font-fredoka text-uppercase" aria-label="Navbar">
    <div class="container-fluid">
        <a href="{{route('homepage')}}">
            <img src="/media/logocarrello.png" class="navbar-brand" width="100px">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarDropdown"
            aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse mt-2" id="navbarDropdown">
            <ul class="navbar-nav me-auto mb-2 mb-md-0">

                {{-- AREA REVISOR --}}
                @if (Auth::check() && Auth::user()->is_revisor)
                <ul class="navbar-nav d-none d-lg-block d-xl-block">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle position-relative" href="#" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <span
                                class="position-absolute top-0 end-0 badge rounded-pill {{App\Models\Article::toBeRevisionedCount() ? "
                                bg-danger badge-rev" : "bg-success" }} ">{{App\Models\Article::toBeRevisionedCount()}}
                            </span>
                            {{__('ui.revisor_zone')}}
                        </a>
                        <ul class=" dropdown-menu shadow border-0 dropdown-layout text-center px-2">

                                <a class="nav-link @if(Route::is('revisor.index')) active @endif"
                                    href="{{route('revisor.index')}}">
                                    {{__('ui.your_dashboard')}}
                                </a>

                                <a class=" nav-link @if(Route::is('revisor.archive')) active @endif"
                                    href="{{route('revisor.archive')}}">
                                    {{__('ui.archive')}}
                                </a>
                </ul>
                </li>
            </ul>
            @endif
            </ul>

            {{-- SEARCH --}}

            <ul class="navbar-nav me-auto align-items-center">
                <form action="{{route('article.search')}}" method="GET" class="d-flex me-md-5 mb-3 mb-md-0"
                    role="search">
                    <input id="searchbar" name="searched" class="form-control me-2 rounded-3" type="search"
                        placeholder="{{__('ui.search_articles')}}" aria-label="Cerca">
                    <button class="btn" type="submit"><i
                            class="fa-solid fa-magnifying-glass fs-4 dark-green"></i></button>
                </form>
            </ul>
            {{-- CHOOSE LANGUAGE --}}
            <div class="d-flex align-items-center justify-content-around">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        @switch(app()->getLocale())
                        @case('it')
                        <button class="btn dropdown-toggle border-0" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="{{asset('vendor/blade-flags/language-it.svg')}}" width="32" height="32">
                        </button>
                        <ul class="dropdown-menu shadow border-0 dropdown-layout">
                            <li>
                                <x-_locale lang="en" />
                            </li>
                            <li>
                                <x-_locale lang="es" />
                            </li>
                        </ul>
                        @break
                        @case('es')
                        <button class="btn dropdown-toggle border-0" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="{{asset('vendor/blade-flags/language-es.svg')}}" width="32" height="32">
                        </button>
                        <ul class="dropdown-menu shadow border-0 dropdown-layout">
                            <li>
                                <x-_locale lang="it" />
                            </li>
                            <li>
                                <x-_locale lang="en" />
                            </li>
                        </ul>
                        @break
                        @default
                        <button class="btn dropdown-toggle border-0" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="{{asset('vendor/blade-flags/language-en.svg')}}" width="32" height="32">
                        </button>
                        <ul class="dropdown-menu shadow border-0 dropdown-layout">
                            <li>
                                <x-_locale lang="it" />
                            </li>
                            <li>
                                <x-_locale lang="es" />
                            </li>
                        </ul>
                        @endswitch
                    </li>
                </ul>

                {{-- CREATE ARTICLE --}}
                @auth
                <li class="navbar-nav">
                    <a class="nav-link @if(Route::is('article.create')) active @endif"
                        href="{{route('article.create')}}"><i class="fa-regular me-1 fa-square-plus"></i>
                        {{__('ui.add_articles')}}

                    </a>
                </li>
            </div>
            {{-- LOGIN - LOGOUT - REGISTER --}}
            <div class="d-flex justify-content-around">
                <li class="navbar-nav dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="fa-solid me-1 fa-house"></i> {{__('ui.hi')}} {{ Auth::user()->name }}
                    </a>

                    <ul class="dropdown-menu rounded-0 border-0 dropdown-layout">
                        <li>
                            <a class="dropdown-item" href="{{route('profile')}}">
                                <i class="fa-solid me-1 fa-heart"></i>
                                {{__('ui.favorites')}}
                            </a>
                        </li>

                        <li>
                            <a class="dropdown-item" href="{{route('myArticle')}}">
                                {{__('ui.my_articles')}}
                            </a>
                        </li>

                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li>
                            <a class="dropdown-item" href="#"
                                onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">
                                <i class="fa-solid fa-right-from-bracket"></i>
                                Logout
                            </a>
                        </li>
                        <form id="form-logout" method="POST" action="{{route('logout')}}" class="d-none">@csrf</form>
                    </ul>
                </li>
                @if (Auth::user()->is_revisor)
                <ul class="navbar-nav d-sm-block d-md-block d-lg-none">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle position-relative" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            {{__('ui.revisor_zone')}}
                            <span
                                    class="position-absolute top-0 end-25 badge rounded-pill {{App\Models\Article::toBeRevisionedCount() ? "
                                    bg-danger badge-rev" : "bg-success" }} ">{{App\Models\Article::toBeRevisionedCount()}}
                                </span>
                        </a>
                        <ul class="dropdown-menu shadow border-0 dropdown-layout text-center px-2">
                            <a class="nav-link @if(Route::is('revisor.index')) active @endif"
                                href="{{route('revisor.index')}}">
                                {{__('ui.your_dashboard')}}
                        </a> 
                        
                        <a class=" nav-link @if(Route::is('revisor.archive')) active @endif"
                                    href="{{route('revisor.archive')}}">
                                    {{__('ui.archive')}}
                            </a>
                        </ul>
                    </li>
                </ul>
            </div>
            @endif
            </ul>
            @else
            <li class="navbar-nav">
                <a class="nav-link @if(Route::is('register')) active @endif" href="{{route('register')}}">
                    <i class="fa-solid fa-user-plus"></i>
                    {{__('ui.register')}}
                </a>
            </li>
            <li class="navbar-nav me-2">
                <a class="nav-link @if(Route::is('login')) active @endif" href="{{route('login')}}">
                    <i class="fa-regular fa-user"></i>
                    {{__('ui.login')}}
                </a>
            </li>
            @endauth
            </ul>

        </div>
    </div>
</nav>