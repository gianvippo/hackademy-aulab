<x-layout title="{{$article->title}} | presto.it">
    
    
    @livewire('categories-navbar')
    
    <div class="container my-5 py-5">
        <div class="row bg-presto-light shadow-lg">
            <div class="col-12 col-md-6 p-0">
                @if(count($article->images))
                <div class="card rounded-0 bg-transparent border-0">
                    <ul id="lightSlider">
                        @foreach($article->images as $image)
                        <li data-thumb="{{$image->getUrl(400,400)}}">
                            <img src="{{$image->getUrl(400,400)}}" class="w-100"
                            alt="@if($image->labels)@foreach($image->labels as $key => $label)@if($key === array_key_last($image->labels)){{$label}}@else{{$label}},@endif @endforeach @endif">
                        </li>
                        @endforeach
                    </ul>
                </div>
                @else
                <img src="/media/logocarrello.png" height="200px" class="d-block mx-auto mt-5">
                @endif
            </div>
            
            <div class="col-12 col-md-6">
                <div class="card bg-transparent border-0 rounded-0 p-3 d-flex flex-column align-content-between h-100">
                    <div class="mb-auto">
                        <x-session-messages />
                        <div class="d-flex justify-content-between">
                            <h2 class="fw-bold font-poppins mt-1 mb-3 text-uppercase dark-green fw-bold">
                                {{$article->title}}
                            </h2>
                            @auth
                            @if (Auth::user()->articles_favorite->contains($article->id))
                            <form action="{{route('unlikeArticle', compact('article'))}}" method="POST">
                                @csrf
                                <button type="submit" class="btn ms-3 border-danger rounded-4"> <i
                                    class="fa-solid text-danger fa-heart"></i> </button>
                                </form>
                                @else
                                <form action="{{route('likeArticle', compact('article'))}}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn ms-3 border-danger rounded-4"> <i
                                        class="fa-regular text-danger fa-heart"></i> </button>
                                    </form>
                                    @endif
                                    @endauth
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <a href="{{route('article.indexByCategory', ['category' => $article->category])}}"
                                        class="float-start badge rounded-pill shadow fs-6 bg-success text-decoration-none">
                                        @switch(app()->getLocale())
                                        @case('it')
                                        {{$article->category->nameIt}}
                                        @break
                                        @case('es')
                                        {{$article->category->nameEs}}
                                        @break
                                        @default
                                        {{$article->category->nameEn}}
                                        @endswitch
                                    </a>
                                    <p class="my-auto font-poppins me-lg-3 fs-3 fw-bold p-2 slimy-green">
                                        {{number_format($article->price, 0, ',', '.')}} &euro;
                                    </p>
                                </div>
                                <div class="divider my-2"></div>
                                <div class="d-flex flex-column align-content-between my-3">
                                    <p class="font-poppins fs-5 dark-green fw-bold">{{__('ui.created')}} <span
                                        class="fw-bold font-poppins slimy-green">{{$article->created_at->format('d/m/Y')}}</span>
                                    </p>
                                    <p class="font-poppins fs-5 dark-green fw-bold">{{__('ui.author')}} <span
                                        class="fw-bold font-poppins fst-italic slimy-green">{{$article->user->name}}</span>
                                    </p>
                                    <p class="font-poppins fs-5 fw-bold">{{__('ui.description')}}</h4>
                                        <p class="font-poppins m-0">{{$article->description}}</p>
                                    </div>
                                    <div class="divider my-2"></div>
                                </div>
                                <div class="d-md-flex justify-content-md-between align-items-md-center">
                                    <div class="d-flex align-items-center justify-content-center pt-md-5">
                                        <a href="{{url()->previous() == url()->current() ? route('article.index') : url()->previous() }}"
                                            class="button-custom-redirect">{{__('ui.back')}}</a>
                                        </div>
                                        <div class="text-center pt-md-5">
                                            @auth
                                            @if (Auth::user()->id != $article->user->id)
                                            @livewire('contact-modal', compact('article'))
                                            @endif
                                            @endauth
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
                <script src='https://sachinchoolur.github.io/lightslider/dist/js/lightslider.js'></script>
                <script>
                    $('#lightSlider').lightSlider({ gallery: true, item: 1, loop: true, slideMargin: 0, galleryMargin: 15, thumbMargin: 15, thumbItem: 4 });
                </script>
                
                
            </x-layout>