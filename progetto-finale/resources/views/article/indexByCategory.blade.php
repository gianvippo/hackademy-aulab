<x-layout title="{{$category_name}} | presto.it">
    @livewire('categories-navbar')

    <x-header background="{{$category->image}}">
        {{$category_name}}
    </x-header>

    <div class="container">
        <div class="row justify-content-center text-center mt-5 pt-5">
            @forelse ($articles as $article)
            @livewire('article-card', compact('article'))
            @empty
            <div class="col-12 col-md-8 text-center my-5 py-5">
                <p class="display-5 my-5 py-5 text-uppercase font-poppins fw-bold dark-brown">{{__('ui.no_articles')}}</p>
            </div>
            @endforelse
        </div>
    </div>

</x-layout>