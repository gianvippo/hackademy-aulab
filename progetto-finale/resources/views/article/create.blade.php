<x-layout title="{{__('ui.add_articles')}} | presto.it">
    @livewire('categories-navbar')

    <x-header>
       {{__('ui.add_articles')}}
    </x-header>

    @livewire('create-article-form')

</x-layout>