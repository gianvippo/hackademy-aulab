<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RevisorController;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::get('/work-with-us', [PublicController::class, 'workWithUs'])->name('work_with_us');
Route::get('/search', [PublicController::class, 'searchArticle'])->name('article.search');

Route::get('/favorites', [ProfileController::class, 'profile'])->middleware('auth')->name('profile');
Route::get('/profile/my-article', [ProfileController::class, 'myArticle'])->middleware('auth')->name('myArticle');
Route::delete('/profile/my-article/{article}/delete', [ProfileController::class, 'deleteArticle'])->middleware('auth')->name('deleteArticle');

// CHANGE LANGUAGE
Route::post('/language/{lang}', [PublicController::class, 'setLanguage'])->name('set_language_locale');

Route::get('/index', [ArticleController::class, 'index'])->name('article.index');
Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
Route::get('/article/show/{article}', [ArticleController::class, 'show'])->name('article.show');
Route::post('/article/show/{article}/like', [ProfileController::class, 'likeArticle'])->middleware('auth')->name('likeArticle');
Route::post('/article/show/{article}/unlike', [ProfileController::class, 'unlikeArticle'])->middleware('auth')->name('unlikeArticle');
Route::get('/{category}', [ArticleController::class, 'indexByCategory'])->name('article.indexByCategory');

Route::get('/revisor/index', [RevisorController::class, 'index'])->middleware('isRevisor')->name('revisor.index');
Route::get('/revisor/archive', [RevisorController::class, 'archive'])->middleware('isRevisor')->name('revisor.archive');
Route::patch('/revisor/{article}/accept', [RevisorController::class, 'acceptArticle'])->middleware('isRevisor')->name('revisor.accept_article');
Route::patch('/revisor/{article}/reject', [RevisorController::class, 'rejectArticle'])->middleware('isRevisor')->name('revisor.reject_article');
Route::patch('/revisor/{article}/cancel', [RevisorController::class, 'cancelAction'])->name('revisor.cancel_action');

Route::post('/request/revisor', [RevisorController::class, 'becomeRevisor'])->middleware('auth')->name('become_revisor');
Route::get('/make/revisor/{user}', [RevisorController::class, 'makeRevisor'])->middleware('isAdmin')->name('make_revisor');


